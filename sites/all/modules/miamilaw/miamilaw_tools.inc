<?php

/***
 * @file
 * MiamiLaw Tools
 */

/**
 * Provides some utilities shared between modules in miamilaw
 */
class MiamiLawTools {

    public static function generateDropdown($tag, $name, &$form, $node_type){
        $key = $tag . $name;
        $form[$key] = array(
            '#type' => 'select',
            '#title' => t('Node ' . ucfirst($name) . ' Column'),
            '#default_value' => isset($form_state['values'][$key]) ? $form_state['values'][$key] : variable_get($key, -1),
            '#options' => MiamiLawTools::getFieldTypes($node_type),
            '#description' => t('The selected column for ' . $name .' title.'),
            '#required' => true,
            '#prefix' => '<div id="' . str_replace('_', '-', $tag) . $name . '-replace">',
            '#suffix' => '</div>',
            '#validated' => true,
        );
    }

    public static function getWeightListKeyed($min=-100, $max=100){
        $values = array();
        for ($i = $min; $i <= $max; $i++){
            $values[$i] = $i;
        }
        return $values;
    }

    public static function getNodeTypes(){
        $query = db_select('node_type')->fields('node_type', array('type', 'name',));
        $options = $query->execute()->fetchAllKeyed();
        return $options;
    }

    public static function getNodes($node_type, $status, $options=array()){
        $efq = new EntityFieldQuery();
        $query = $efq->entityCondition('entity_type', 'node')
            ->propertyCondition('type', $node_type)
            ->propertyCondition('status', $status);

        if (count($options)){
            if (isset($options['order'])){
                $query->fieldOrderBy($options['order']['field'], 'value', $options['order']['direction']);
            }
        }
        $results = $query->execute();
        $nodes = array();
        if (isset($results['node'])){
            $nodes = node_load_multiple(array_keys($results['node']));
        }
        return $nodes;
    }

    public static function getNodesKeyed($node_type, $options=array()){
        $nodes = MiamiLawTools::getNodes($node_type, 1, $options);
        $list = array();
        foreach($nodes as $node){
            $list[$node->nid] = $node->title;
        }
        return $list;
    }

    public static function getMenusKeyed(){
        $query = db_select('menu_custom')->fields('menu_custom', array('menu_name', 'title',));
        return $query->execute()->fetchAllKeyed();
    }

    public static function getTaxonomyVocab(){
        $query = db_select('taxonomy_vocabulary')->fields('taxonomy_vocabulary', array('vid', 'name',));
        $options = $query->execute()->fetchAllKeyed();
        return $options;
    }

    public static function getFieldTypes($type){
        $query = db_select('field_config_instance')->fields('field_config_instance', array('field_id', 'field_name',));
        $options = $query
            ->condition('bundle', $type)
            ->execute()->fetchAllKeyed();
        return $options;
    }

    public static function getFieldValue($entity, $field, $id){
        return db_select($entity, 'f')
            ->fields('f', array($field,))
            ->condition('id', $id)
            ->execute()->fetchField();
    }

    /* Borrowed from https://api.drupal.org/comment/52278#comment-52278 */
    public static function getTIDFromName($name){
        $query = new EntityFieldQuery;
        $result = $query
            ->entityCondition('entity_type', 'taxonomy_term')
            ->propertyCondition('name', $name)
            ->propertyCondition('vid', 4) // 4 == Vid of the known vocabulary.
            ->execute();

        //Extract the Tid from the results. (Note the difference from the above comment, it was missing the return var which is required)
        return array_shift($result['taxonomy_term'])->tid;
    }

    /* Get taxonomy term tids by vocabulary name */
    public static function getTIDSFromVocabulary($name){
        $vid = taxonomy_vocabulary_machine_name_load($name)->vid;

        $query = new EntityFieldQuery();
        $result = $query->entityCondition('entity_type', 'taxonomy_term')
            ->propertyCondition('vid', $vid)
            ->execute();
        $values = array();
        foreach ($result['taxonomy_term'] as $taxonomy){
            $values[] = $taxonomy->tid;
        }

        return $values;
    }

    /* Validate taxonomy url tid is valid for given vocabulary */
    public static function validateVocabularyPath($name){
        if (arg(0) == 'taxonomy' && arg(1) == 'term'){
            $ids = MiamiLawTools::getTIDSFromVocabulary($name);
            if (in_array(arg(2), $ids)) {
                return true;
            }
        }
        return false;
    }

    public static function getRenderedNodeValue($node, $field){
        $items = field_get_items('node', $node, $field);
        if (sizeof($items) > 1) {
            $rendered = '<ul>';
            foreach($items as $item){
                $field_value = field_view_value('node', $node, $field, $item);
                $rendered .= '<li>' . render($field_value) . '</li>';
            }
            $rendered .= '</ul>';
            return $rendered;
        }
        $field_value = field_view_value('node', $node, $field, $items[0]);
        return render($field_value);
    }

    public static function getMenuAsList($menu){
        $tree = menu_tree_all_data($menu);
        $list = array();
        foreach ($tree as $branch){
            //echo "<pre>" . print_r($branch) . "</pre>";
            $list[] = array('url' => url($branch['link']['link_path']), 'name' => $branch['link']['title']);
        }
        //echo "<pre>" . print_r($list) . "</pre>";
        return $list;
    }

  /**
   * @param $entity_type
   * @param $entity
   * @param $field_name
   * @param $column_name
   * @return array|bool
   */
    public static function fieldGetColumnValues($entity_type, $entity, $field_name, $column_name) {
      if ($field_items = field_get_items($entity_type, $entity, $field_name)) {
        $values = array();
        foreach ($field_items as $item) {
          if (isset($item[$column_name])) {
            $values[] = $item[$column_name];
          }
        }
        return empty($values) ? FALSE : $values;
      }

      return FALSE;
    }

    /**
     * Get Collapsible Block
     * @param $title
     * @param $content
     * @return string
     */
    public static function createCollapsibleBlock($title, $content){
        $anchor = str_replace(' ', '_', strtolower($title));
        return <<<EOT
                    <div class="collapsible-content" id="$anchor">
                        <div class="collapse-action">
                            <h3>
                                <i class="fa fa-chevron-right"></i>
                                &nbsp;$title
                            </h3>
                        </div>
                        <div class="collapse-target" style="display: hidden;">
                            $content
                        </div>
                    </div>
EOT;
    }

    /**
     * @param $title
     * @param $description
     * @param $variable_name
     * @return array
     */
    public static function createFormImageField($title, $description, $variable_name){
        return array(
            '#type' => 'media',
            '#title' => t($title),
            '#tree' => TRUE,
            '#input' => TRUE,
            '#description' => t($description),
            '#default_value' => variable_get($variable_name, null),
            '#media_options' => array(
                'global' => array(
                    'types' => array(
                        'image' => 'image',
                    ),
                    'enabledPlugins' => array(
                        'upload' => 'upload',
                        'library' => 'library',
                        'http' => 'http',
                    ),
                    'schemes' => array(
                        'public' => 'public',
                    ),
                    'file_directory' => 'media',
                    'file_extensions' => 'png gif jpg jpeg',
                    'max_filesize' => '2 MB',
                    'uri_scheme' => 'public',
                ),
            ),
        );
    }

    /**
     * @param $name
     * @return bool|string
     */
    public static function getImageURLFromVariable($name){
        $image = variable_get($name);
        if (is_array($image) && file_load($image['fid'])) {
            return file_create_url(file_load($image['fid'])->uri);
        } else if (is_numeric($image) && (int)$image > 0){
            return file_create_url(file_load($image)->uri);
        }
        return '';
    }

    /**
     * Turn all URLs in clickable links.
     *
     * Borrowed from: https://gist.github.com/jasny/2000705
     *
     * @param string $value
     * @param array $protocols http/https, ftp, mail, twitter
     * @param array $attributes
     * @internal param string $mode normal or all
     * @return string
     */
    public static function linkify($value, $protocols = array('http', 'mail', 'twitter'), array $attributes = array('target' => '_blank'))
    {
        // Link attributes
        $attr = '';
        foreach ($attributes as $key => $val) {
            $attr = ' ' . $key . '="' . htmlentities($val) . '"';
        }

        $links = array();

        // Extract existing links and tags
        $value = preg_replace_callback('~(<a .*?>.*?</a>|<.*?>)~i', function ($match) use (&$links) { return '<' . array_push($links, $match[1]) . '>'; }, $value);

        // Extract text links for each protocol
        foreach ((array)$protocols as $protocol) {
            switch ($protocol) {
                case 'http':
                case 'https':   $value = preg_replace_callback('~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { if ($match[1]) $protocol = $match[1]; $link = $match[2] ?: $match[3]; return '<' . array_push($links, "<a $attr href=\"$protocol://$link\">$link</a>") . '>'; }, $value); break;
                case 'mail':    $value = preg_replace_callback('~([^\s<]+?@[^\s<]+?\.[^\s<]+)(?<![\.,:])~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"mailto:{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
                case 'twitter': $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"https://twitter.com/" . ($match[0][0] == '@' ? '' : 'search/%23') . $match[1]  . "\">{$match[0]}</a>") . '>'; }, $value); break;case 'twitter': $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"https://twitter.com/" . ($match[0][0] == '@' ? '' : 'search/%23') . $match[1]  . "\">{$match[0]}</a>") . '>'; }, $value); break;
                case 'facebook': $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"https://www.facebook.com/" . ($match[0][0] == '@' ? '' : 'hashtag/') . $match[1]  . "\">{$match[0]}</a>") . '>'; }, $value); break;
                case 'instagram': $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"https://www.instagram.com/" . ($match[0][0] == '@' ? '' : 'explore/tags/') . $match[1]  . "\">{$match[0]}</a>") . '>'; }, $value); break;
                default:        $value = preg_replace_callback('~' . preg_quote($protocol, '~') . '://([^\s<]+?)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { return '<' . array_push($links, "<a $attr href=\"$protocol://{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
            }
        }

        // Insert all link
        return preg_replace_callback('/<(\d+)>/', function ($match) use (&$links) { return $links[$match[1] - 1]; }, $value);
    }

    /**
     * @param $node
     * @return array
     */
    public static function getNodeContents($node){
        $values = array();
        $values['image'] = file_create_url($node->{HeadlinesData::$image}[LANGUAGE_NONE][0]['uri']);
        $values['title'] = $node->title;
        $values['tags'] = $node->{HeadlinesData::$tags}[LANGUAGE_NONE];

        switch ($node->type){
            case 'miami_law_document':
                $values['link'] = file_create_url($node->field_document_file[LANGUAGE_NONE][0]['uri']);
                $term = taxonomy_term_load($node->field_document_icon['und'][0]['tid']);
                $values['font-class'] = strip_tags($term->description);
                $values['date'] = $node->field_document_date[LANGUAGE_NONE][0]['value'];
                $text = MiamiLawTools::removeMediaMarkup($node->field_document_content[LANGUAGE_NONE][0]['value']);
                $values['content'] = views_trim_text(array(
                        'max_length' => variable_get('articles_landing_content_length', 300),
                        'ellipsis' => true,
                        'word_boundary' => true,
                        'html' => true,
                    ),
                    strip_tags($text, '<p>')
                );
                break;
            case 'miami_law_external_link':
                $values['link'] = url($node->field_external_link_url[LANGUAGE_NONE][0]['url']);
                $term = taxonomy_term_load($node->field_external_link_icon['und'][0]['tid']);
                $values['font-class'] = strip_tags($term->description);
                $values['date'] = $node->field_external_link_date[LANGUAGE_NONE][0]['value'];
                $text = MiamiLawTools::removeMediaMarkup($node->field_external_link_content[LANGUAGE_NONE][0]['value']);
                $values['content'] = views_trim_text(array(
                        'max_length' => variable_get('articles_landing_content_length', 300),
                        'ellipsis' => true,
                        'word_boundary' => true,
                        'html' => true,
                    ),
                    strip_tags($text, '<p>')
                );
                break;
            case 'miami_law_article':
            case 'press_article':
            default:
                $values['link'] = url(drupal_get_path_alias('node/'.$node->nid));
                $term = taxonomy_term_load($node->field_article_link_icon['und'][0]['tid']);
                $values['font-class'] = strip_tags($term->description);
                $values['date'] = $node->field_article_date_authored[LANGUAGE_NONE][0]['value'];
                $text = MiamiLawTools::removeMediaMarkup($node->field_article_content[LANGUAGE_NONE][0]['value']);
                $values['content'] = views_trim_text(array(
                        'max_length' => variable_get('articles_landing_content_length', 300),
                        'ellipsis' => true,
                        'word_boundary' => true,
                        'html' => true,
                    ),
                    strip_tags($text, '<p>')
                );
                break;
        }

        return $values;
    }

    /**
     * @param $text
     * @return mixed
     */
    static function removeMediaMarkup($text){
        $value = preg_replace('/(\[.*\])*/', '', $text);
        return $value;
    }

    /**
     * @param $month
     * @return bool|string
     */
    static function convertMonthNumberToName($month){
        if (is_string($month)){
            return DateTime::createFromFormat('!m', $month)->format('F');
        }
        return date('F', mktime(0,0,0,$month,10));
    }

    /**
     * @param $year
     * @param $month
     * @param string $lead
     * @return string
     */
    static function createNewsURL($year, $month, $lead = 'news'){
        if (!empty($month)) {
            return url(drupal_get_path_alias($lead . '/' . strval($year) . '/' . strtolower(MiamiLawTools::convertMonthNumberToName($month))));
        }
        return url(drupal_get_path_alias($lead . '/' . strval($year)));
    }

    /**
     * @param $nid
     * @param null $vid
     * @param bool $reset
     * @return bool|mixed
     */
    static function nodeLoad($nid, $vid = NULL, $reset = FALSE){
        if ($nid <= 0){
            return false;
        }
        return node_load($nid, $vid, $reset);
    }


    /**
     * Check if Array is an associative array or not (i.e. 0 - #)
     * Borrowed from http://stackoverflow.com/a/173479
     * @param array $arr
     * @return bool
     */
    static function isArrayAssociative(array $arr){
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}

/**
 * Class MiamiLawTaggedContent
 */
class MiamiLawTaggedContent {
    private $tid;
    private $node_types;
    private $year;
    private $month;
    private $filters = array('all_link' => 'year', 'tags' => true, 'months' => true, 'lead_url' => 'news');
    private $nodes;

    /**
     * @param $term_name
     * @param $year
     * @param $month
     * @param array $node_types
     * @internal param $id
     */
    public function __construct($term_name, $year, $month, $node_types = array('miami_law_article', 'miami_law_document', 'miami_law_external_link')){
        $this->year = $year;
        $this->month = $month;
        $this->updateNodeTypes($term_name, $node_types, $year, $month);
    }

    /**
     * @return bool
     */
    public function hasNodes() {
        return !empty($this->nodes);
    }

    /**
     * @param $term_name
     * @return mixed
     */
    private function getTid($term_name){
        return current(taxonomy_get_term_by_name($term_name))->tid;
    }

    private static function mergeKeys(){
        $arr = func_get_args();
        $num = func_num_args();

        $keys = array();
        $i = 0;
        for ($i=0; $i<$num; ++$i){
            $keys = array_merge($keys, array_keys($arr[$i]));
        }
        $keys = array_unique($keys);

        $merged = array();

        foreach ($keys as $key){
            $merged[$key] = array();
            for($i=0; $i<$num; ++$i){
                $merged[$key][] = isset($arr[$i][$key]) ? $arr[$i][$key] : null;
            }
        }
        return $merged;
    }

    /**
     * @param $tid
     * @param $node_types
     * @param $year
     * @return array
     */
    public static function getNodes($tid, $node_types, $year){
        $first_of_month = date('Y-m-d 00:00:00', strtotime($year . '-1-1'));
        $last_of_month = date('Y-m-t 23:59:59', strtotime($year . '-12-1'));
        $nodes = array();
        foreach ($node_types as $node_type) {
            $efq = new EntityFieldQuery();
            $efq = $efq->entityCondition('entity_type', 'node');
            $efq = $efq->propertyCondition('type', array($node_type), 'IN');
            $efq->fieldCondition('field_headlines_tags', 'tid', $tid);
            $efq->propertyCondition('status', 1);
            if ($node_type == 'miami_law_article' || $node_type == 'press_article') {
                $efq->fieldCondition('field_article_date_authored', 'value', array($first_of_month, $last_of_month), 'BETWEEN');
            } else if ($node_type == 'miami_law_external_link'){
                $efq->fieldCondition('field_external_link_date', 'value', array($first_of_month, $last_of_month), 'BETWEEN');
            } else if ($node_type == 'miami_law_document'){
                $efq->fieldCondition('field_document_date', 'value', array($first_of_month, $last_of_month), 'BETWEEN');
            } else {
                $efq->fieldCondition('created', array($first_of_month, $last_of_month), 'BETWEEN');
            }
            $results = $efq->execute();
            if (!isset($results['node'])) {
                continue;
            }
            $new_nodes = node_load_multiple(array_keys($results['node']));
            $nodes = array_merge($nodes, $new_nodes);
        }

        if (empty($nodes)){
            return array();
        }

        @uksort($nodes, 'compare_node_by_date');

        $list = array();

        foreach ($nodes as $node){
            if ($node->status == NODE_NOT_PUBLISHED) continue;
            $date = '';
            if ($node->type == 'miami_law_article' || $node->type == 'press_article') {
                $date = $node->field_article_date_authored[LANGUAGE_NONE][0]['value'];
            } else if ($node->type == 'miami_law_external_link'){
                $date = $node->field_external_link_date[LANGUAGE_NONE][0]['value'];
            } else if ($node->type == 'miami_law_document'){
                $date = $node->field_document_date[LANGUAGE_NONE][0]['value'];
            } else {
                $date = date('Y-m-d', $node->created);
            }
            $datetime = new DateTime($date);
            $node_year = $datetime->format('Y');
            $node_month = $datetime->format('n');
            $list[$node_year][$node_month][] = MiamiLawTools::getNodeContents($node);
        }
        krsort($list[$year]);

        return $list;
    }

    /**
     * @param $term_name
     * @param $node_types
     */
    public function updateNodeTypes($term_name, $node_types){
        $this->tid = $this->getTid($term_name);
        $this->node_types = $node_types;
        $this->nodes = self::getNodes($this->tid, $this->node_types, $this->year);
    }

    /**
     * @param $key
     * @param $value
     */
    public function updateFilter($key, $value){
        $this->filters[$key] = $value;
    }

    /**
     * @param $id
     * @param $title
     * @param int $max_items
     * @internal param $year
     * @internal param $month
     * @internal param $tid
     * @internal param array $node_type
     * @internal param array $filters
     * @internal param $subject
     * @return string
     */
    public function createBlock($id, $title, $max_items=4){

        $block = self::createHeader($id, $this->tid, $title, $this->year, $this->month, $max_items, $this->node_types, $this->filters, $this->nodes);

        $block .= self::createContent($id, $this->tid, $this->nodes, $this->year, $this->month, $max_items);

        return $block;
    }

    public function createYearBlock($id, $max_items=4){
        $content = '';
        $this->updateFilter('months', false);
        $this->updateFilter('all_link', 'month');
        foreach ($this->nodes[$this->year] as $key => $month) {
            $this->month = $key;
            $content .= $this->createBlock($id . '-' . $key, MiamiLawTools::convertMonthNumberToName($key) . ', ' . $this->year, 4);
        }
        return $content;
    }

    /** See more link
     * @param $id
     * @param $tid
     * @param $title
     * @param $year
     * @param $month
     * @param $max_items
     * @param $node_types
     * @param $filters
     * @param $nodes
     * @internal param $tid
     * @internal param $node_type
     * @internal param $filters
     * @return string
     */
    public static function createHeader($id, $tid, $title, $year, $month, $max_items, $node_types, $filters, $nodes){

        $tag_form_content = drupal_get_form('tagged_content_tags_drop_down', $id, $title, $tid, $year, $month, $max_items, $node_types, $filters, $nodes);
        $month_form_content = drupal_get_form('tagged_content_months_drop_down', $id, $title, $tid, $year, $month, $max_items, $node_types, $filters, $nodes);

        $tags_form = drupal_render($tag_form_content);
        $months_form = drupal_render($month_form_content);

        //$all_title = strtoupper($title);

        $content = <<<EOT
        <div class="tagged-content-header" id="tagged-content-header-replace-$id">
            <div class="tc-title"><h1>$title</h1></div>
EOT;
        if (isset($filters['all_link'])) {
            $all_url = MiamiLawTools::createNewsURL($year, $filters['all_link'] == 'year' ? '' : $month, $filters['lead_url']);
            $content .= '<div class="tc-all"><a href="' . $all_url . '">SEE ALL</a></div>';
        }
        if ($filters['tags']) {
            $content .= '<div class="tc-filter-tags"><i>FILTER BY TAG:</i> ' . $tags_form . '</div>';
        }
        if ($filters['months']) {
            $content .= '<div class="tc-filter-months"><i>FILTER BY MONTH:</i> ' . $months_form . '</div>';
        }
        $content .= '</div>';
        return $content;
    }

    /**
     * @param $id
     * @param $tid
     * @param $nodes
     * @param $year
     * @param $month
     * @param int $max_items
     * @internal param int $default_tid
     * @internal param $node_type
     * @return string
     */
    public static function createContent($id, $tid, $nodes, $year, $month, $max_items){

        $content = '<div class="tagged-contents" id="box-replace-' . $id . '">';

        $current_nodes = array();

        if (isset($nodes[strval($year)][$month])){
            $current_nodes = $nodes[strval($year)][$month];
        }
        /* else {
            $find_year = $year;
            $find_month = $month;
            for ($i = $find_year; $i > ($find_year - 5); $i--){
                $find_month--;
                if ($find_month < 0){
                    $find_month = 12;
                }
                if (isset($nodes[strval($find_year)][$find_month])){
                    $current_nodes = $nodes[strval($find_year)][$find_month];
                    break;
                }
            }
        }*/

        if (!isset($current_nodes)) {
            $name = '';
            if ($tid != -1){
                $name = taxonomy_term_load($tid)->name;
            }
            return $content . '<div class="message">No content filed under tag: ' . $name . '</div></div>';
        }

        $nodeCount = 0;

        $totalNodes = count($current_nodes);

        foreach ($current_nodes as $key => $values) {
            $tags = '';
            if ($values['tags']) {
                $tags .= '<ul>';
                $count = 0;
                foreach ($values['tags'] as $tag) {
                    $term = taxonomy_term_load($tag['tid']);
                    $uri = taxonomy_term_uri($term);
                    $url = file_create_url(drupal_get_path_alias($uri['path']));
                    if (empty($term->name)) {
                        continue;
                    }
                    $tags .= '<a href="' . $url . '"><li>' . $term->name . '</li></a>';
                    if (++$count >= 6) break;
                }
                $tags .= '</ul>';
            }

            $datetime = new DateTime($values['date']);
            $date = $datetime->format('l, F d, Y');

            $content .= <<<EOT
                        <div class="tagged-content-container">
                             <div class="tc-image"><img src="{$values['image']}" /></div>
                             <div class="tc-title"><a href="{$values['link']}">{$values['title']}</a></div>
                             <div class="tc-date">$date</div>
                             <div class="tc-content">{$values['content']}</div>
                             <div class="tc-tags">$tags</div>
                        </div>
EOT;
            if (++$nodeCount >= $max_items) break;
        }

        $content .= '</div>';

        return $content;
    }

    /**
     * @param $nodes
     * @param $year
     * @param $month
     * @internal param $tid
     * @internal param $node_type
     * @return array
     */
    public static function getTagList($nodes, $year, $month){

        $terms_select = array();

        if (isset($nodes[$year][$month])) {
            foreach ($nodes[$year][$month] as $node){
                $tags = $node['tags'];
                if ($tags) {
                    foreach ($tags as $tag) {
                        $term = taxonomy_term_load($tag['tid']);
                        if (empty($term->name)) {
                            continue;
                        }
                        if (isset($term->depth)) {
                            $terms_select[$term->tid] = str_repeat('-', $term->depth) . $term->name;
                        } else {
                            $terms_select[$term->tid] = $term->name;
                        }
                    }
                }
            }
        }

        return $terms_select;
    }

    /**
     * @param $nodes
     * @param $year
     * @return mixed
     */
    public static function getMonthList($nodes, $year){

        $months = cal_info(0)['months'];

        foreach ($months as $key => $value){
            if (isset($nodes[$year][$key])) {
                $months[$key] = $value . ', ' . $year;
            } else {
                unset($months[$key]);
            }
        }

        return $months;
    }

}

/**
 * @param $form
 * @param $form_state
 * @internal param $id
 * @internal param $title
 * @internal param $tid
 * @internal param $year
 * @internal param $month
 * @internal param $max_items
 * @internal param $node_type
 * @internal param $filters
 * @internal param $nodes
 * @internal param $node_types
 * @internal param $node_type
 * @return mixed
 */
function tagged_content_tags_drop_down($form, &$form_state) {
    $args = $form_state['build_info']['args'];
    $id = $args[0];
    $title = $args[1];
    $tid = $args[2];
    $year = $args[3];
    $month = $args[4];
    $max_items = $args[5];
    $node_type = $args[6];
    $filters = $args[7];
    $nodes = $args[8];

    $form = array();

    $form['#attributes']['class'][] = 'drop-down';

    $terms_select = MiamiLawTaggedContent::getTagList($nodes, $year, $month);

    $form['tid'] = array(
        '#type' => 'select',
        '#title' => '',
        '#default_value' => $tid,
        '#options' => $terms_select,
        '#ajax' => array(
            'callback' => 'tc_tags_drop_down_callback',
            'effect' => 'fade',
            'progress' => array('type' => 'none'),
        )
    );

    $form['replace_id'] = array(
        '#type' => 'hidden',
        '#value' => $id,
    );

    $form['title'] = array(
        '#type' => 'hidden',
        '#value' => $title,
    );

    $form['month'] = array(
        '#type' => 'hidden',
        '#value' => $month,
    );

    $form['year'] = array(
        '#type' => 'hidden',
        '#value' => $year,
    );

    $form['max_items'] = array(
        '#type' => 'hidden',
        '#value' => $max_items,
    );

    $form['node_type'] = array(
        '#type' => 'hidden',
        '#value' => $node_type,
    );

    $form['filters'] = array(
        '#type' => 'hidden',
        '#value' => $filters,
    );

    return $form;
}


/**
 * @param $form
 * @param $form_state
 * @internal param $id
 * @internal param $title
 * @internal param $tid
 * @internal param $year
 * @internal param $month
 * @internal param $max_items
 * @internal param $node_type
 * @internal param $filters
 * @internal param $nodes
 * @internal param $tid
 * @return mixed
 */
function tagged_content_months_drop_down($form, &$form_state) {

    $args = $form_state['build_info']['args'];
    $id = $args[0];
    $title = $args[1];
    $tid = $args[2];
    $year = $args[3];
    $month = $args[4];
    $max_items = $args[5];
    $node_type = $args[6];
    $filters = $args[7];
    $nodes = $args[8];

    $form = array();
    $form['#attributes']['class'][] = 'drop-down';

    $months = MiamiLawTaggedContent::getMonthList($nodes, $year);

    $form['month'] = array(
        '#type' => 'select',
        '#title' => '',
        '#default_value' => $month,
        '#options' => $months,
        '#ajax' => array(
            'callback' => 'tc_months_drop_down_callback',
            'effect' => 'fade',
            'progress' => array('type' => 'none'),
        )
    );

    $form['replace_id'] = array(
        '#type' => 'hidden',
        '#value' => $id,
    );

    $form['title'] = array(
        '#type' => 'hidden',
        '#value' => $title,
    );

    $form['tid'] = array(
        '#type' => 'hidden',
        '#value' => $tid
    );

    $form['year'] = array(
        '#type' => 'hidden',
        '#value' => $year,
    );

    $form['max_items'] = array(
        '#type' => 'hidden',
        '#value' => $max_items,
    );

    $form['node_type'] = array(
        '#type' => 'hidden',
        '#value' => $node_type,
    );

    $form['filters'] = array(
        '#type' => 'hidden',
        '#value' => $filters,
    );

    return $form;
}

/**
 * @param $form
 * @param $form_state
 * @return array
 */
function tc_tags_drop_down_callback($form, $form_state){
    $id = $form_state['values']['replace_id'];
    $title = $form_state['values']['title'];
    $tid = $form_state['values']['tid'];
    $month = $form_state['values']['month'];
    $year = $form_state['values']['year'];
    $max_items = $form_state['values']['max_items'];
    $node_type = $form_state['values']['node_type'];
    $filters = $form_state['values']['filters'];

    $nodes = MiamiLawTaggedContent::getNodes($tid, $node_type, $year);

    return array(
        '#type' => 'ajax',
        '#commands' => array(
            ajax_command_replace('#tagged-content-header-replace-' . $id, MiamiLawTaggedContent::createHeader($id, $tid,$title,$year,$month,$max_items, $node_type, $filters, $nodes)),
            ajax_command_invoke('.form-select', 'chosen', array(
                'disable_search' => true,
                'width' => 'auto',
            )),
            ajax_command_replace('#box-replace-' . $id, MiamiLawTaggedContent::createContent($id, $tid, $nodes, $year, $month, $max_items, $node_type)),
        ),
    );
}

/**
 * Call back
 * @param $form
 * @param $form_state
 * @internal param $max_items
 * @return array
 */
function tc_months_drop_down_callback($form, $form_state){
    $id = $form_state['values']['replace_id'];
    $title = $form_state['values']['title'];
    $tid = $form_state['values']['tid'];
    $month = $form_state['values']['month'];
    $year = $form_state['values']['year'];
    $max_items = $form_state['values']['max_items'];
    $node_type = $form_state['values']['node_type'];
    $filters = $form_state['values']['filters'];

    $nodes = MiamiLawTaggedContent::getNodes($tid, $node_type, $year);

    return array(
        '#type' => 'ajax',
        '#commands' => array(
            ajax_command_replace('#tagged-content-header-replace-' . $id, MiamiLawTaggedContent::createHeader($id, $tid,$title,$year,$month,$max_items,$node_type, $filters, $nodes)),
            ajax_command_invoke('.form-select', 'chosen', array(
                'disable_search' => true,
                'width' => 'auto',
            )),
            ajax_command_replace('#box-replace-' . $id, MiamiLawTaggedContent::createContent($id, $tid, $nodes, $year, $month, $max_items, $node_type)),
        ),
    );
}

/**
 *
 */
function compare_node_by_date($a, $b){
    /*$node1 = node_load($a);
    $node2 = node_load($b);*/
    $node1 = $a;
    $node2 = $b;
    $date1 = $date2 = null;
    if ($node1->type == 'miami_law_article' || $node1->type == 'press_article') {
        $date1 = $node1->field_article_date_authored[LANGUAGE_NONE][0]['value'];
    } else if ($node1->type == 'miami_law_external_link'){
        $date1 = $node1->field_external_link_date[LANGUAGE_NONE][0]['value'];
    } else if ($node1->type == 'miami_law_document'){
        $date1 = $node1->field_document_date[LANGUAGE_NONE][0]['value'];
    } else {
        $date1 = date('Y-m-d', $node1->created);
    }
    if ($node2->type == 'miami_law_article' || $node2->type == 'press_article') {
        $date2 = $node2->field_article_date_authored[LANGUAGE_NONE][0]['value'];
    } else if ($node2->type == 'miami_law_external_link'){
        $date2 = $node2->field_external_link_date[LANGUAGE_NONE][0]['value'];
    } else if ($node2->type == 'miami_law_document'){
        $date2 = $node2->field_document_date[LANGUAGE_NONE][0]['value'];
    } else {
        $date2 = date('Y-m-d', $node2->created);
    }
    return (new DateTime($date1)) > (new DateTime($date2));
    //return strcmp($date2, $date1);
};
