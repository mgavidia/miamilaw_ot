/**
 * Created by mgavidia on 8/15/14.
 */
(function ($) {
    Drupal.behaviors.miamilaw_flickr_photos = {
        attach: function (context, settings) {
            if (!$("[class^=galleria]").length){
                return;
            }
            $(window).bind('load', function() {
                $('.galleria').once('flickr-show', function () {
                    var left = $('.l-media').find('.right').first().find('img');
                    var height = 0;
                    left.each(function(index){
                        var item = $(this);
                        //console.log('Got index: ' + index);
                        height = height + item.height();
                    });
                    //console.log("Height: " + height);
                    Galleria.loadTheme(Drupal.settings.miamilaw_flickr_photos.path);
                    Galleria.run('.galleria', {
                        responsive: true,
                        debug: false,
                        height: height / 3.9,
                        transition: 'flash',
                        flickr: 'set:' + Drupal.settings.miamilaw_flickr_photos.photoset,
                        showInfo: false,
                        wait: 10000
                    });
                });
            });
        }
    }
})(jQuery);