<?php

/**
 * @file
 * Digital Measures functions and utilities
 */

// Helpers
require_once dirname(__FILE__) . '/../../miamilaw_tools.inc';

/**
 * Digital Measures API v4
 *
 * * User List:
 * /login/service/v4/User/INDIVIDUAL-ACTIVITIES-University/COLLEGE:School of Law
 *
 * Search for username:
 * /login/service/v4/User/INDIVIDUAL-ACTIVITIES-University/COLLEGE:School of Law?firstName=name&lastName=name
 *
 * Personal Data:
 * /login/service/v4/SchemaData/INDIVIDUAL-ACTIVITIES-University/USERNAME:username
 * @param $name
 * @param array $options
 * @return string
 */
function digital_measures_get_resource_uri($name, $options=array()){
    $service = variable_get('dm_server') . variable_get('dm_service');
    switch ($name){
        case 'list':
            $service .= '/User/INDIVIDUAL-ACTIVITIES-University/COLLEGE:School%20of%20Law';
            break;
        case 'search':
            $service .= '/User/INDIVIDUAL-ACTIVITIES-University/COLLEGE:School%20of%20Law?firstName=' . $options['first'] .'&lastName=' . $options['last'];
            break;
        case 'data':
            $service .= '/SchemaData/INDIVIDUAL-ACTIVITIES-University/USERNAME:' . $options['username'];
            break;
        case 'relaxng':
            $service .= '/SchemaData:relaxng/INDIVIDUAL-ACTIVITIES-University/COLLEGE:School%20of%20Law/';
            break;
        default:
            break;
    }
    return $service;
}

function digital_measures_get_entity_id($name){
    $query = db_select('miamilaw_dm_schema_entity', 'f')
        ->fields('f')
        ->condition('name', $name, '=')
        ->execute()
        ->fetch();
    if ($query){
        return $query->id;
    }
    return 0;
}

function digital_measures_get_option_name($id){
    $query = db_select('miamilaw_dm_schema_option', 'f')
        ->fields('f')
        ->condition('id', $id, '=')
        ->execute()
        ->fetch();
    if ($query){
        return $query->name;
    }
    return 0;
}

/**
 * @param bool $ids
 * @param bool $remove_used
 * @return mixed
 */
function digital_measures_get_entities_keyed($ids=false, $remove_used=false){
    $query = db_select('miamilaw_dm_schema_entity', 'f')
        ->fields('f');
    if ($remove_used){
        $removed = db_select('miamilaw_dm_display_format', 'f')
            ->fields('f')
            ->execute()->fetchAllKeyed(9, 0);
        if ($removed) {
            $pci = digital_measures_get_entity_id('PCI');
            if (isset($removed[$pci])){
                unset($removed[$pci]);
            }
            $query->condition('id', array_keys($removed), 'NOT IN');
        }
    }
    $results = $query->execute();
    $values = $ids ? $results->fetchAllKeyed(0, 2) : $results->fetchAllKeyed(2, 3);

    return $values;
}

function digital_measures_get_user_fields_keyed(){
    $keys = db_select('miamilaw_dm_user_field', 'f')
        ->fields('f')
        ->execute()
        ->fetchAllKeyed(0, 1);

    return $keys;
}

function digital_measures_get_schema_option_keyed($id){
    $options = db_select('miamilaw_dm_schema_option', 'f')
        ->fields('f')
        ->condition('entity_id', $id, '=')
        ->execute()
        ->fetchAllKeyed(0, 1);

    return $options;
}

/**
 * @param $xml
 * @return mixed
 */
function digital_measures_xml_to_array($xml){
    if (!$xml){
        return false;
    }
    // Convert xml string to array
    $content = simplexml_load_string($xml, "SimpleXMLElement", LIBXML_NOCDATA);
    $json = json_encode($content);
    return json_decode($json, true);
}

/**
 * Convert xml to json
 * @param $xml
 * @return bool|string
 */
function digital_measures_xml_to_json($xml){
    if (!$xml){
        return false;
    }
    // Convert xml string to array
    $content = simplexml_load_string($xml, "SimpleXMLElement", LIBXML_NOCDATA);
    return json_encode($content);
}

/**
 * @param $json
 * @return bool|mixed
 */
function digital_measures_json_to_array($json){
    if (!$json){
        return false;
    }
    return json_decode($json, true);
}

/**
 * @param $url
 * @return bool|mixed
 */
function digital_measures_fetch_url($url){
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        //CURLOPT_SSL_VERIFYPEER => false),
        CURLOPT_USERPWD => variable_get('dm_username') . ':' . variable_get('dm_password'),
        CURLOPT_ENCODING => 'gzip',
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_RETURNTRANSFER => true,
    ));
    $response = curl_exec($curl);

    if (curl_errno($curl)){
        $errorMessage = curl_error($curl);
        //echo "<pre>" . $errorMessage . "</pre>";
        curl_close($curl);
        return false;
    }

    $info = curl_getinfo($curl);

    return $response;
}

/**
 * @param $value
 * @param string $empty
 * @return string
 */
function digital_measures_extract_array_value($value, $empty=''){
    return isset($value) && !empty($value) ? $value : $empty;
}

/**
 * @param $entity
 * @param $user_entity
 * @param $string
 * @return mixed
 */
function digital_measures_format_string($entity, $user_entity, $string){
    if (!$entity){
        return $string;
    }
    $options = db_select('miamilaw_dm_schema_option', 'f')
        ->fields('f')
        ->condition('entity_id', $entity->id, '=')
        ->execute()
        ->fetchAll();

    $data = digital_measures_json_to_array($user_entity->data);
    foreach ($options as $option){
        $context = '{' . $option->name . '}';
        $url = '/\{URL=' . $option->name . '\}/';
        $optional = '/\{OPTIONAL=' . $option->name . '\}/';
        $if = '/\{IF ' . $option->name . '=(.*?)\}/';
        $ifnot = '/\{IFNOT ' . $option->name . '=(.*?)\}/';
        $matches = array();
        if (strpos($string, $context) !== false){
            // replace
            $value = digital_measures_extract_array_value($data[$option->name]);
            $string = str_replace($context, $value, $string);
        }
        if (preg_match($url, $string)){
            $value = digital_measures_extract_array_value($data[$option->name]);
            if ($value) {
                $string = preg_replace($url, '<a href="' . $value . '" target="_blank">', $string);
                $string = preg_replace('\{\/URL\}', '</a>', $string);
            } else {
                // No value remove link
                $string = preg_replace($url, '', $string);
                $string = preg_replace('/\{\/URL\}/', '', $string);
            }
        }
        if (preg_match($optional, $string)){
            $value = digital_measures_extract_array_value($data[$option->name]);
            if ($value) {
                $string = preg_replace($optional, '', $string);
                $string = preg_replace('/\{\/OPTIONAL\}/', '', $string);
            } else {
                // No value remove optional content
                $remove_opt = '/\{OPTIONAL=' . $option->name . '\}.*\{\/OPTIONAL\}/';
                $string = preg_replace($remove_opt, '', $string);
            }
        }
        if (preg_match($if, $string, $matches)){
            $value = digital_measures_extract_array_value($data[$option->name]);
            if ($value && $matches[1] === $value) {
                $string = preg_replace($if, '', $string);
                $string = preg_replace('/\{\/IF\}/', '', $string);
            } else {
                // No value remove if content
                $remove_opt = '/\{IF ' . $option->name . '=(.*?)\}.*\{\/IF\}/';
                $string = preg_replace($remove_opt, '', $string);
            }
        }
        if (preg_match($ifnot, $string, $matches)){
            $value = digital_measures_extract_array_value($data[$option->name]);
            if ($value && $matches[1] !== $value) {
                $string = preg_replace($ifnot, '', $string);
                $string = preg_replace('/\{\/IFNOT\}/', '', $string);
            } else {
                // No value remove if content
                $remove_opt = '/\{IFNOT ' . $option->name . '=(.*?)\}.*\{\/IFNOT\}/';
                $string = preg_replace($remove_opt, '', $string);
            }
        }
    }

    return $string;
}

/**
 * @param $entity
 * @param $user_entities
 * @return string
 */
function digital_measures_format_data($entity, $user_entities, $make_collapse = true){
    $formatted_content = '';
    if ($entity && $user_entities) {
        $formatter = db_select('miamilaw_dm_display_format', 'f')
            ->fields('f')
            ->condition('entity_id', $entity->id, '=')
            ->execute()
            ->fetchAssoc();
        if ($formatter) {
            $content = '';
            if ($formatter['sort_id'] != NULL){
                $direction = $formatter['sort_direction'];
                $option = digital_measures_get_option_name($formatter['sort_id']);
                uasort($user_entities, function($a, $b) use ($direction, $option){
                    $a_data = digital_measures_json_to_array($a->data);
                    $b_data = digital_measures_json_to_array($b->data);
                    if ($direction === '1'){
                        // DESC
                        return -strcmp($a_data[$option], $b_data[$option]);
                    } else {
                        // ASC
                        return strcmp($a_data[$option], $b_data[$option]);
                    }
                });
            }
            $count = 0;
            foreach ($user_entities as $e){
                $content .= digital_measures_format_string($entity, $e, $formatter['string']);
                $limit = intval($formatter['display_limit']);
                if ($limit > -1){
                    if (++$count === $limit){
                        break;
                    }
                }
            }
            if (!empty($content)){
                if ($make_collapse) {
                    $title = !empty($formatter['title']) ? $formatter['title'] : $entity->description;
                    $formatted_content = MiamiLawTools::createCollapsibleBlock($title, $content);
                } else {
                    $formatted_content = $content;
                }
            }
        }
    }
    return $formatted_content;
}

function array_append_user_data(&$user, $index, $content){
    if (!isset($user[$index])){
        $user[$index] = '';
    }
    $user[$index] .= $content;
}

/**
 * @param $nid
 * @return array
 * @internal param $first
 * @internal param $last
 */
function digital_measures_get_faculty_data($nid){
    $user = db_select('miamilaw_dm_user', 'f')
        ->fields('f')
        ->condition('nid', $nid, '=')
        ->execute()
        ->fetchAssoc();

    if ($user){
        $query = db_select('miamilaw_dm_schema_entity', 'f');
        $query->join('miamilaw_dm_display_format', 'd', 'f.id = d.entity_id');
        $entities = $query
            ->fields('f')
            ->fields('d', array('override_id'))
            ->condition('d.enabled', '1', '=')
            ->orderBy('d.weight', 'ASC')
            ->execute()
            ->fetchAll();
        if ($entities) {
            foreach ($entities as $entity) {
                $user_entities = db_select('miamilaw_dm_user_entity', 'e')
                    ->fields('e', array('username', 'entity_id', 'data'))
                    ->condition('username', $user['username'], '=')
                    ->condition('entity_id', $entity->id, '=')
                    ->execute()
                    ->fetchAll();
                if ($user_entities) {
                    if ($entity->override_id) {
                        switch ($entity->override_id) {
                            case '1':
                                array_append_user_data($user, 'title', digital_measures_format_data($entity, $user_entities, false));
                                break;
                            case '2':
                                array_append_user_data($user, 'email', digital_measures_format_data($entity, $user_entities, false));
                                break;
                            case '3':
                                array_append_user_data($user, 'phone', digital_measures_format_data($entity, $user_entities, false));
                                break;
                            case '4':
                                array_append_user_data($user, 'room', digital_measures_format_data($entity, $user_entities, false));
                                break;
                            case '5':
                                array_append_user_data($user, 'degrees', digital_measures_format_data($entity, $user_entities, false));
                                break;
                            case '6':
                                array_append_user_data($user, 'biography', digital_measures_format_data($entity, $user_entities, false));
                                break;
                            case '7':
                                array_append_user_data($user, 'publications', digital_measures_format_data($entity, $user_entities));
                                break;
                            case '8':
                                array_append_user_data($user, 'media', digital_measures_format_data($entity, $user_entities));
                                break;
                            case '9':
                                array_append_user_data($user, 'presentations', digital_measures_format_data($entity, $user_entities));
                                break;
                            case '10':
                                array_append_user_data($user, 'office', digital_measures_format_data($entity, $user_entities, false));
                                break;
                            case '11':
                                array_append_user_data($user, 'websites', digital_measures_format_data($entity, $user_entities, false));
                                break;
                            case '12':
                                array_append_user_data($user, 'social', digital_measures_format_data($entity, $user_entities, false));
                                break;
                            default:
                                break;
                        }
                    } else {
                        array_append_user_data($user, 'formatted', digital_measures_format_data($entity, $user_entities));
                    }
                }
            }
        }

        return $user;
    }

    return array();
}

/**
 * @return array
 */
function digital_measures_get_entities_as_rows(){
    $query = db_select('miamilaw_dm_user', 'f');
    $query->join('miamilaw_dm_user_entity', 'p', 'f.username = p.username');
    $query->groupBy('f.username');
    $query->fields('f', array('username', 'nid', 'created', 'changed'));
    $query->fields('p', array('data'));
    $query->condition('p.entity_id', digital_measures_get_entity_id('PCI'), '=');
    $results = $query->execute();

    $rows = array();

    $nids = array(
        '#type' => 'select',
        '#title' => t('Faculty Nodes'),
        '#title_display' => 'invisible',
        '#empty_value' => '',
        '#empty_option' => t('- Not Assigned -'),
        '#default_value' => '',
        '#options' => array('- Not Assigned -') + MiamiLawTools::getNodesKeyed('faculty_member', array(
            'order' => array(
                'field' => 'field_faculty_last_name',
                'direction' => 'ASC',
            ))),
    );

    while ($record = $results->fetch()){
        /**
         * 'username' => t('Username (ID)'),
        'name' => t('Name'),
        'nid' => t('Node ID'),
        'created' => t('Date Created'),
        'last-update' => t('Last Updated'),
         */
        $data = digital_measures_json_to_array($record->data);
        $fname = isset($data['FNAME']) && !is_array($data['FNAME']) ? $data['FNAME'] : '';
        $mname = isset($data['MNAME']) && !is_array($data['MNAME']) ? $data['MNAME'] : '';
        $lname = isset($data['LNAME']) && !is_array($data['LNAME']) ? $data['LNAME'] : '';
        $name = $fname . ' ' . $mname . ' ' . $lname;

        $select = $nids;
        $select['#title'] = $name;
        $select['#value'] = $record->nid;
        $select['#name'] = 'nid[' . $record->username . ']';

        $rows[] = array(
            'username' => $record->username,
            'name' => $name,
            //'nid' => $record->nid === NULL ? 'Not set' :  $record->nid,
            'nid' => array(
                'data' => $select,
            ),
            'created' => date('m/d/Y', $record->created),
            'last-update' => date('m/d/Y', $record->changed),
        );
    }

    return $rows;
}

function digital_measures_get_entity_options_as_rows($entity_id){
    $options = db_select('miamilaw_dm_schema_option', 'f')
        ->fields('f')
        ->condition('entity_id', $entity_id, '=')
        ->execute()
        ->fetchAll();

    $rows = array();
    if ($options){
        foreach ($options as $option){
            $rows[] = array($option->name, $option->description);
        }
    }

    return $rows;
}

/**
 * @param $username
 * @param $entity_id
 * @param $entity_data
 */
function digital_measures_save_entity($username, $entity_id, $entity_data){
    $content = array(
        'id' => $entity_data['@attributes']['id'],
        'username' => $username,
        'entity_id' => $entity_id,
        'data' => json_encode($entity_data)
    );
    //echo "<pre> Content: " . print_r($content) . " </pre>";
    $check = db_select('miamilaw_dm_user_entity', 'f')
        ->fields('f')
        ->condition('username', $content['username'], '=')
        ->condition('id', $content['id'], '=')
        ->condition('entity_id', $content['entity_id'], '=')
        ->execute()
        ->fetchAssoc();
    if ($check) {
        // Exists update
        db_update('miamilaw_dm_user_entity')
            ->fields($content)
            ->condition('username', $content['username'], '=')
            ->condition('id', $content['id'], '=')
            ->condition('entity_id', $content['entity_id'], '=')
            ->execute();
    } else {
        // Add new row
        db_insert('miamilaw_dm_user_entity')
            ->fields($content)
            ->execute();
    }
}

/**
 * @param $username
 * @param $entity_name
 * @param $data
 */
function digital_measures_save_entities($username, $entity_name, $data){
    if (!$data){
        return;
    }
    $entity = db_select('miamilaw_dm_schema_entity', 'f')
        ->fields('f')
        ->condition('name', $entity_name, '=')
        ->execute()
        ->fetchAssoc();
    if ($entity) {
        if (!MiamiLawTools::isArrayAssociative($data)) {
            foreach ($data as $e) {
                digital_measures_save_entity($username, $entity['id'], $e);
            }
        } else {
            digital_measures_save_entity($username, $entity['id'], $data);
        }
    }
}

/**
 * @param $first
 * @param $last
 * @return null
 */
function digital_measures_find_faculty_nid($first, $last){
    $efq = new EntityFieldQuery();
    $query = $efq->entityCondition('entity_type', 'node')
        ->propertyCondition('type', 'faculty_member')
        ->fieldCondition('field_faculty_first_name', 'value', $first, '=')
        ->fieldCondition('field_faculty_last_name', 'value', $last, '=')
        ->range(0, 1)
        ->execute();
    if (isset($query['node'])){
        $node = array_shift(node_load_multiple(array_keys($query['node'])));
        return property_exists($node, 'nid') ? $node->nid : NULL;
    }
    return NULL;
}

/**
 * @param $username
 * @param $nid
 * @return bool
 */
function digital_measures_update_nid($username, $nid){
    $user = array(
        'username' => $username,
        'nid' => $nid === 0 ? NULL : $nid,
        'changed' => REQUEST_TIME,
    );
    $member = db_select('miamilaw_dm_user', 'f')
        ->fields('f')
        ->condition('username', $user['username'], '=')
        ->execute()
        ->fetchAssoc();

    if ($member){
        // Update nid
        if (strval($member['nid']) === strval($nid)){
            // do nothing
            return false;
        }
        // Update the nid
        db_update('miamilaw_dm_user')
            ->fields($user)
            ->condition('username', $user['username'], '=')
            ->execute();
        return true;
    }

    return false;
}

/**
 *
 */
function digital_measures_update_data(){
    // Schema entity and options
    $entities = digital_measures_fetch_relaxng();
    foreach ($entities as $name => $entity){
        $entityfields = array(
            'name' => $name,
            'type' => $entity['type'],
            'description' => $entity['description'],
        );
        $check = db_select('miamilaw_dm_schema_entity', 'f')
            ->fields('f')
            ->condition('name', $name, '=')
            ->execute()
            ->fetchAssoc();
        $id = NULL;
        if ($check){
            // Exists update
            db_update('miamilaw_dm_schema_entity')
                ->fields($entityfields)
                ->condition('name', $name, '=')
                ->execute();
            $id = $check['id'];
        } else {
            // Add new entry
            $id = db_insert('miamilaw_dm_schema_entity')
                ->fields($entityfields)
                ->execute();
        }
        foreach ($entity['options'] as $tagname => $tagdesc){
            $tagfields = array(
                'name' => $tagname,
                'description' => $tagdesc,
                'entity_id' => $id,
            );
            $tag_check = db_select('miamilaw_dm_schema_option', 'f')
                ->fields('f')
                ->condition('name', $tagname, '=')
                ->condition('entity_id', $id, '=')
                ->execute()
                ->fetchAssoc();
            if ($tag_check){
                db_update('miamilaw_dm_schema_option')
                    ->fields($tagfields)
                    ->condition('name', $tagname, '=')
                    ->condition('entity_id', $id, '=')
                    ->execute();
            } else {
                db_insert('miamilaw_dm_schema_option')
                    ->fields($tagfields)
                    ->execute();
            }
        }
    }

    // User information
    $url = digital_measures_get_resource_uri('list');
    if ($list = digital_measures_xml_to_array(digital_measures_fetch_url($url))){
        foreach ($list['User'] as $key => $value){
            $user_uri = digital_measures_get_resource_uri('data', array('username' => $value['@attributes']['username']));
            if ($data = digital_measures_xml_to_array(digital_measures_fetch_url($user_uri))){
                // Get base user
                $firstname = $data['Record']['PCI']['FNAME'];
                $lastname = $data['Record']['PCI']['LNAME'];
                $user = array(
                    'username' => $data['Record']['@attributes']['username'],
                    'nid' => digital_measures_find_faculty_nid($firstname, $lastname),
                    'created' => REQUEST_TIME
                );
                $check = db_select('miamilaw_dm_user', 'f')
                    ->fields('f')
                    ->condition('username', $user['username'], '=')
                    ->execute()
                    ->fetchAssoc();
                if ($check){
                    // Exists update
                    unset($user['created']);
                    $user['changed'] = REQUEST_TIME;
                    if ($check['nid'] != NULL){
                        unset($user['nid']);
                    }
                    db_update('miamilaw_dm_user')
                        ->fields($user)
                        ->condition('username', $user['username'], '=')
                        ->execute();
                } else {
                    // Add new entry
                    db_insert('miamilaw_dm_user')
                        ->fields($user)
                        ->execute();
                }

                foreach ($data['Record'] as $entity_name => $item){
                    if ($entity_name === '@attributes') continue;
                    digital_measures_save_entities($user['username'], $entity_name, $item);
                }
            }
        }
    }
}

/**
 * @param $name
 * @return int
 */
function digital_measures_relaxng_get_type($name){
    switch($name){
        case 'optional': return 0;
        case 'zeroOrMore': return 1;
    }

    return 0;
}

/**
 * @param $type
 * @param $item
 * @param bool $parent
 * @return array
 */
function digital_measures_relaxng_get_attributes($type, $item, $parent=true){
    $attributes = $item->element->attributes();
    $entity = (string)$attributes['name'];
    $description = (string)$attributes['text'];
    if (!$parent){
        return array(
            $entity => $description,
        );
    }
    $content = array(
        $entity => array(
            'type' => $type,
            'description' => $description,
            'options' => array(),
        ),
    );
    foreach ($item->element->interleave->optional as $element){
        $content[$entity]['options'] += digital_measures_relaxng_get_attributes($type, $element, false);
    }
    return $content;
}

/**
 * @return array
 */
function digital_measures_fetch_relaxng(){
    $url = digital_measures_get_resource_uri('relaxng');
    $xml = digital_measures_fetch_url($url);

    // Replace : indexes so that simplexmlelement can read the tags
    $xml = str_replace('r:', '', str_replace('dms:', '', $xml));

    $reader = new XMLReader();
    $reader->XML($xml);

    while ($reader->read()){
        // Get Schema Entities and the relative tags
        if ($reader->name === 'define' && $reader->getAttribute('name') === 'Record-INDIVIDUAL-ACTIVITIES-University'){
            $content = array();
            $node = new SimpleXMLElement($reader->readOuterXml());
            foreach ($node->element->interleave->optional as $item){
                $type = digital_measures_relaxng_get_type('optional');
                $content += digital_measures_relaxng_get_attributes($type, $item);
            }
            foreach ($node->element->interleave->zeroOrMore as $item){
                $type = digital_measures_relaxng_get_type('zeroOrMore');
                $content += digital_measures_relaxng_get_attributes($type, $item);
            }
            //echo "<pre>" . print_r($content, true) . "</pre>";
            return $content;
        }
    }

    return array();
}