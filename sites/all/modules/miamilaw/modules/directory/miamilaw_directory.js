/**
 * Created by mgavidia on 09/17/14.
 */

(function ($) {

    function Type(container, title){
        this.container = container;
        this.enabled = false;
        this.child = null;
        this.title = title;
    };

    Type.prototype.enable = function(override){
        override = typeof override !== 'undefined' ? override : 'fa-times';
        if (!this.enabled){
            this.enabled = true;
            var content = $('<li>' + this.title + ' <span class="fa ' + override + '"></span></li>');
            this.container.append(content);
            this.child = content;
        }
    };

    Type.prototype.clear = function(){
        this.enabled = false;
        if (this.child){
            this.child.remove();
        }
    };

    Type.prototype.verify = function(check, types){
        return (this.enabled && $.inArray(check, types) != -1);
    };

    Drupal.behaviors.miamilaw_directory = {
        attach: function (context, settings) {

            var form = $('.form-chosen');
            form.once('directory-no-search', function(){
                form.chosen({
                    disable_search: true,
                    width: '200px',
                    placeholder_text_single: 'POSITION'
                });
            });

            var expertise_form = $('.form-chosen-expertise');
            expertise_form.once('faculty-no-search', function(){
                expertise_form.chosen({
                    disable_search: true,
                    width: '200px',
                    placeholder_text_single: 'FACULTY EXPERTISE'
                });
            });

            var initTypes = function(container){
                var personnel_types = Drupal.settings.miamilaw_directory.personnel_types;
                var lookups = Drupal.settings.miamilaw_directory.personnel_names;
                var list = {};
                personnel_types.forEach(function(type){
                    var name = lookups[type];
                    //console.log('Name: ' + name + ' val: ' + type);
                    if (name.indexOf('Admin') > -1){
                        name = name.replace('Admin ', '');
                    }
                    list[type] = new Type(container, name );
                });

                return list;
            };

            var initExpertise = function(container){
                var expertise_types = Drupal.settings.miamilaw_expertise.expertise_types;
                var lookups = Drupal.settings.miamilaw_expertise.expertise_names;
                var list = {};
                expertise_types.forEach(function(type){
                    var name = lookups[type];
                    list[type] = new Type(container, name);
                });

                return list;
            };

            // Create objects
            var members = $('.content .members');
            var objects = [];
            members.find('.member').each(function(){
                var member = $(this);
                var types = member.attr('types').split(",");
                //member.css('display', 'none');
                objects.push({
                    block: member,
                    last_name: member.attr('last-name'),
                    first_name: member.attr('first-name'),
                    middle_name: member.attr('middle-name'),
                    room_number: member.attr('room-number'),
                    phone_number: member.attr('phone-number'),
                    types: types
                });
            });

            // Filters
            var filter = $('.content .filter');
            var searchbox = $('#name-filter', filter);
            var button = $('.buttons .button', filter);
            var enabled = $('.enabled ul', filter);

            // Filter options
            var filterOptions = {
                currentText: '',
                all: new Type(enabled, 'All'),
                clear: new Type(enabled, 'Remove All Filters'),
                types: initTypes(enabled),
                expertise: initExpertise(enabled),
                setAll: function(){
                    this.clear.enable('fa-eraser');
                    this.all.enable();
                    $.each(this.types, function(key, type){
                       type.clear();
                    });
                },
                clearAll: function(){
                    this.all.clear();
                    this.clear.clear();
                    $.each(this.types, function(key, type){
                        type.clear();
                    });
                    $.each(this.expertise, function(key, type){
                        type.clear();
                    });
                },
                expertiseSet: function(){
                    var on = false;
                    $.each(this.expertise, function(key, type){
                        if (type.enabled){
                            on = true;
                        }
                    });
                    return on;
                },
                inputVerification: function(){
                    var filtersEnabled = false;
                    $.each(this.types, function(key, type){
                        filtersEnabled |= type.enabled;
                    });
                    $.each(this.expertise, function(key, type){
                        filtersEnabled |= type.enabled;
                    });
                    if (!filtersEnabled && !this.all.enabled){
                        this.setAll();
                    }
                },
                remove: function(obj){
                    if (this.all.child && this.all.child.get(0) == obj.get(0)){
                        this.all.clear();
                    }

                    if (this.clear.child && this.clear.child.get(0) == obj.get(0)){
                        this.clearAll();
                    }

                    $.each(this.types, function(key, type){
                        if (type.child && type.child.get(0) == obj.get(0)){
                            type.clear();
                        }
                    });

                    $.each(this.expertise, function(key, type){
                        if (type.child && type.child.get(0) == obj.get(0)){
                            type.clear();
                        }
                    });
                    this.enabledFilterCheck();
                },
                enabledFilterCheck: function(){
                    var total = enabled.find('li').size();
                    if (total == 1){
                        // should be only the remove filters lets clear it
                        this.clear.clear();
                    }
                }
            };

            // Run filter
            var checkFilters = function(){

                var item = $(this);

                var lastName = item.attr('last-name').toLowerCase();
                var firstName = item.attr('first-name').toLowerCase();
                var middleName = item.attr('middle-name').toLowerCase();
                var room = item.attr('room-number');
                var phone = item.attr('phone-number');
                var types = item.attr('types').split(",");
                var expertise = item.attr('expertise').split(",");
                var enabled = false;

                $.each(filterOptions.types, function(key, type){
                    if (type.verify(key, types)){
                        enabled = true;
                    }
                });

                if (filterOptions.all.enabled){
                    enabled = true;
                }

                if (filterOptions.expertiseSet()) {
                    enabled = false;
                    $.each(filterOptions.expertise, function (key, type) {
                        if (type.verify(key, expertise)) {
                            enabled = true;
                        }
                    });
                }

                // Finally text matches
                if ((lastName.indexOf(filterOptions.currentText) == -1) &&
                    (firstName.indexOf(filterOptions.currentText) == -1) &&
                    (middleName.indexOf(filterOptions.currentText) == -1)){
                    enabled = false;
                }

                return enabled;
            };

            if ($(".form-chosen option[value='all']").length > 0) {
                filterOptions.setAll();
            }

            // initiate isotope
            var container = members.isotope({
                itemSelector: '.member',
                getSortData: {
                  lastname: '[last-name]'
                },
                sortBy: 'lastname',
                filter: checkFilters
            });

            // Search box
            searchbox.bind('change paste keyup', function(){
                filterOptions.currentText = $(this).val().toLowerCase();

                // Check first run
                filterOptions.inputVerification();

                // Reload
                container.isotope({ filter: checkFilters });
            });

            // Add filter
            form.on('change', function(evt, params){
                var buttonType = form.val();//$(this).attr('class').split(' ')[1];
                //console.log('Type:'+ buttonType);
                if (buttonType == 'all'){
                    filterOptions.setAll();
                    container.isotope({ filter: checkFilters });
                }

                $.each(filterOptions.types, function(key, type){
                    if (buttonType == key){
                        filterOptions.all.clear();
                        filterOptions.clear.enable('fa-eraser');
                        type.enable();
                        container.isotope({ filter: checkFilters });
                    }
                });

                form.val('').trigger('chosen:updated');
            });

            expertise_form.on('change', function(evt, params){
                var buttonType = expertise_form.val();//$(this).attr('class').split(' ')[1];
                //console.log('Type:'+ buttonType);
                $.each(filterOptions.expertise, function(key, type){
                    if (buttonType == key){
                        filterOptions.clear.enable('fa-eraser');
                        type.enable();
                        container.isotope({ filter: checkFilters });
                    }
                });

                expertise_form.val('').trigger('chosen:updated');
            });

            $('option').once('enabled options', function() {
                var enabled = false;
                $('option').each(function () {
                    var btn = $(this);
                    if (btn.attr('on') == '1') {
                        if (!enabled){
                            filterOptions.clear.enable('fa-eraser');
                            enabled = true;
                        }
                        //console.log('val: ' + btn.val() + ' type: ' + filterOptions.types[btn.val()]);
                        filterOptions.types[btn.val()].enable();
                    }
                });
                if (enabled){
                    container.isotope({ filter: checkFilters });
                }
            });

            // Remove filters
            $('li', enabled).live('click', function(){
                filterOptions.remove($(this));
                container.isotope({ filter: checkFilters });
            });

            /* Check if something was input by drupal */
            if (searchbox.val().length > 0){
                searchbox.trigger('change');
            }
        }
    }
})(jQuery);
