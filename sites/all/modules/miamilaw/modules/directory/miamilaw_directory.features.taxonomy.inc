<?php
/**
 * @file
 * miamilaw_directory.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function miamilaw_directory_taxonomy_default_vocabularies() {
  return array(
    'expertise' => array(
      'name' => 'Expertise',
      'machine_name' => 'expertise',
      'description' => 'Faculty Expertise Tags',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -6,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'personnel_types' => array(
      'name' => 'Personnel Types',
      'machine_name' => 'personnel_types',
      'description' => 'Personnel Types associated with Faculty and Staff',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -4,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
