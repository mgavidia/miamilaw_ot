<?php
/**
 * @file
 * miamilaw_directory.features.inc
 */

/**
 * Implements hook_node_info().
 */
function miamilaw_directory_node_info() {
  $items = array(
    'admin_staff_member' => array(
      'name' => t('Admin / Staff Member'),
      'base' => 'node_content',
      'description' => t('Admin or Staff Member'),
      'has_title' => '1',
      'title_label' => t('Full Name'),
      'help' => '',
    ),
    'faculty_member' => array(
      'name' => t('Faculty Member'),
      'base' => 'node_content',
      'description' => t('Faculty Member Content Type.'),
      'has_title' => '1',
      'title_label' => t('Full Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
