<?php

require_once dirname(__FILE__) . '/../../miamilaw_tools.inc';

include_once 'miamilaw_magazine.features.inc';

/**
 * Help hook.
 *
 * @param path
 *   Which path of the site we're using to display help
 * @param arg
 *   Array that holds the current path as returned from arg() function
 */
function miamilaw_magazine_help($path, $arg){
  switch ($path) {
    case "admin/help#miamilaw_magazine":
      return '<p>' . t("This module provides the displaying of Magazine content types.") . '</p>';
      break;
  }
}

/**
 * hook_block_info
 */
function miamilaw_magazine_block_info() {
  $blocks = array(
      'miamilaw_magazine_date' => array(
          'info' => t('Miami Law Magazine Date'),
          'cache' => DRUPAL_CACHE_PER_ROLE,
      ),
      'miamilaw_magazine_menu' => array(
          'info' => t('Miami Law Magazine Menu'),
          'cache' => DRUPAL_CACHE_PER_ROLE,
      ),
      'miamilaw_magazine_feature_story' => array(
          'info' => t('Miami Law Magazine Feature Story'),
          'cache' => DRUPAL_CACHE_PER_ROLE,
      ),
      'miamilaw_magazine_links_social' => array(
          'info' => t('Miami Law Magazine Links and Social'),
          'cache' => DRUPAL_CACHE_PER_ROLE,
      ),
      'miamilaw_magazine_stories' => array(
          'info' => t('Miami Law Magazine Stories'),
          'cache' => DRUPAL_CACHE_PER_ROLE,
      ),
      'miamilaw_magazine_additional' => array(
          'info' => t('Miami Law Magazine Additional'),
          'cache' => DRUPAL_CACHE_PER_ROLE,
      ),
      'miamilaw_magazine_tags' => array(
          'info' => t('Miami Law Magazine Tags'),
          'cache' => DRUPAL_CACHE_PER_ROLE,
      ),
  );
  return $blocks;
}

/**
 * Permissions
 */
function miamilaw_magazine_permission(){
    return array(
        'magazine admin' => array(
            'title' => t('Administrate Magazine Pages'),
            'description' => t('Admin Magazine content.'),
        ),
    );
}

/**
 * hook_block_view
 */
function miamilaw_magazine_block_view($delta = ''){
    $node = magazine_get_node();
    $values = magazine_extract_node_contents($node);
    switch ($delta) {
        case 'miamilaw_magazine_date':
            $block['subject'] = t('Miami Law Magazine Date');
            return $block;
            break;

        case 'miamilaw_magazine_menu':
            $block['subject'] = t('Miami Law magazine Menu');
            // block
            $block['content'] = magazine_create_menu();
            return $block;
            break;

        case 'miamilaw_magazine_feature_story':
            if (magazine_is_year_landing() || !count($values)){
                return array();
            }
            $block['subject'] = t('Miami Law magazine Feature Story');
            // block
            $heading = variable_get('magazine_feature_story_title', 'Feature Story');
            $block['content'] = <<<EOT
            <div class="magazine-feature-story">
                <div class="magazine-feature-story-heading">
                    $heading
                </div>
                <a href="{$values['url']}" class="magazine-feature-story-image">
                    <img class="ui rounded image" src="{$values['image']}" />
                </a>
                <a href="{$values['url']}" class="magazine-feature-story-title">
                    {$values['title']}
                </a>
                <a href="{$values['url']}" class="magazine-feature-story-text">
                    {$values['text']}
                </a>
            </div>
EOT;
            return $block;
            break;

        case 'miamilaw_magazine_links_social':
            if (magazine_is_year_landing() || !count($values)){
                return array();
            }
            $block['subject'] = t('Miami Law magazine Links and Social');
            // Buttons
            $buttons = '';
            foreach ($node->field_magazine_button_links[LANGUAGE_NONE] as $button){
                $url = url($button['url']);
                //$classes = explode(' ', $button['attributes']['class']);
                $color = '';
                $icon = '';
                $matches = array();
                preg_match('/(fa\-.+?)? /i', $button['attributes']['class'], $matches);
                //if (count($classes) > 1){
                if (count($matches)){
                    //$icon = '<i class="fa fa-lg ' . $classes[0] . '" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;';
                    //$color = $classes[1];
                    $icon = '<i class="fa fa-lg ' . $matches[0] . '" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;';
                    $color = str_replace($matches[0], '', $button['attributes']['class']);
                } else { //if (count($classes) === 1) {
                    $color = $button['attributes']['class']; //$classes[0];
                }
                $buttons .= <<<EOT
                    <a class="fluid ui $color button" href="$url">$icon{$button['title']}</a>
EOT;
            }
            // Printed Version
            $printed_title = variable_get('magazine_print_version_title', 'Explore the printed version');
            $printed_iframe = $node->field_magazine_printed_version[LANGUAGE_NONE][0]['value'];

            // Social
            $connect_title = variable_get('magazine_social_buttons_title', 'Connect with us');

            // Social links
            $social_buttons = '';
            for ($i = 0; $i < 6; $i++){
                if ($url = variable_get('magazine-social-' . strval($i) . '-url')){
                    $image = MiamiLawTools::getImageURLFromVariable('magazine-social-' . strval($i) . '-image');
                    $social_buttons .= <<<EOT
                        <a class="item" href="$url">
                            <img class="ui image" src="$image">
                        </a>
EOT;
                }
            }

            // block
            $block['content'] = <<<EOT
                <div class="magazine-links-social">
                    <div class="magazine-links-social-buttons">
                        <div class="fluid ui vertical buttons">
                            $buttons
                        </div>
                    </div>
                    <div class="magazine-links-social-printed">
                        <div class="magazine-links-social-printed-title">
                            $printed_title
                        </div>
                        <div class="magazine-links-social-printed-embed">
                            $printed_iframe
                        </div>
                    </div>
                    <div class="magazine-links-social-connect">
                        <div class="magazine-links-social-connect-title">
                            $connect_title
                        </div>
                        <div class="magazine-links-social-connect-links">
                            <div class="ui link horizontal list mini images">
                                $social_buttons
                            </div>
                        </div>
                    </div>
                </div>
EOT;
            return $block;
            break;

        case 'miamilaw_magazine_stories':
            if (magazine_is_year_landing() || !count($values)){
                return array();
            }
            $list = get_magazine_sub_stories($values['year'], $values['quarter-key']);
            $block['subject'] = t('Miami Law magazine Stories');
            // block
            $block['content'] = <<<EOT
                <div class="magazine-stories">
                    <div class="ui three stackable centered cards">
EOT;
            foreach ($list as $story) {
                $block['content'] .= <<<EOT
                            <div class="card">
                                <a class="image" href="{$story['url']}">
                                    <img src="{$story['image']}">
                                </a>
                                <div class="content">
                                    <a class="header" href="{$story['url']}">{$story['title']}</a>
                                    <a class="description" href="{$story['url']}">
                                        {$story['text']}
                                    </a>
                                </div>
                                <div class="extra content">
                                    {$story['tags']}
                                </div>
                            </div>
EOT;
            }
            $block['content'] .= <<<EOT
                    </div>
                </div>
EOT;
            return $block;
            break;

        case 'miamilaw_magazine_additional':
            if (magazine_is_year_landing() || !count($values)){
                return array();
            }
            $block['subject'] = t('Miami Law magazine Additional');

            $list = get_magazine_extra_content($values['year'], $values['quarter-key']);
            if (!count($list)){
                return array();
            }
            $heading = variable_get('magazine_additional_title', 'Also in this issue');
            // block
            $block['content'] = <<<EOT
                <div class="magazine-additional">
                    <div class="magazine-additional-heading">
                        $heading
                    </div>
                    <div class="ui four stackable centered cards">
EOT;
            foreach ($list as $content) {
                $block['content'] .= <<<EOT
                        <div class="card">
                            <a class="ui centered small circular image" href="{$content['url']}">
                                <img src="{$content['image']}">
                            </a>
                                <!--<img class="ui centered small circular image" src="{$content['image']}">-->
                            <div class="content">
                                <a class="header" href="{$content['url']}">{$content['title']}</a>
                                <a class="description" href="{$content['url']}">
                                    {$content['text']}
                                </a>
                            </div>
                        </div>
EOT;
            }
            $block['content'] .= <<<EOT
                    </div>
EOT;
            return $block;
            break;

        case 'miamilaw_magazine_tags':
            $block['subject'] = t('Miami Law Magazine Tags');
            // block
            if (arg(0) == 'node' && is_numeric(arg(1))) {
                if ($node = MiamiLawTools::nodeLoad(arg(1))) {
                    $title = variable_get('magazine_tags_title');
                    $tags = render(field_view_field('node', $node, 'field_headlines_tags', array('label' => 'hidden')));
                    $block['content'] = <<<EOT
                    <div class="tags">
                        <h3>$title</h3>
                        $tags
                    </div>
EOT;
                }
            }
            return $block;
            break;
    }
}

function magazine_extract_quarter($node){
    if (!$node){
        return '';
    }
    $quarter = '';
    switch ($node->field_magazine_quarter[LANGUAGE_NONE][0]['value']){
        case 'q1': $quarter = 'spring';break;
        case 'q2': $quarter = 'summer';break;
        case 'q3': $quarter = 'fall';break;
        case 'q4': $quarter = 'winter';break;
    }
    return $quarter;
}

function magazine_extract_node_contents($node){
    if (!$node){
        return array();
    }
    $quarter = magazine_extract_quarter($node);
    return array(
        'year' => date('Y', strtotime($node->field_magazine_year[LANGUAGE_NONE][0]['value'])),
        'quarter-key' => $node->field_magazine_quarter[LANGUAGE_NONE][0]['value'],
        'quarter' => $quarter,
        'image' => file_create_url($node->field_magazine_story_image[LANGUAGE_NONE][0]['uri']),
        'url' => url($node->field_magazine_story_link[LANGUAGE_NONE][0]['url']),
        'title' => $node->field_magazine_story_title[LANGUAGE_NONE][0]['value'],
        'text' => $node->field_magazine_story_text[LANGUAGE_NONE][0]['value'],
    );
}

/**
 * hook_menu
 */
function miamilaw_magazine_menu(){
  $items = array(
      'admin/config/content/miamilaw/magazine' => array(
          'title' => 'Miami Law Magazine',
          'description' => 'Configuration for Miami Law Magazine',
          'page callback' => 'drupal_get_form',
          'page arguments' => array('miamilaw_magazine_form'),
          'access arguments' => array('magazine admin'),
          'type' => MENU_NORMAL_ITEM,
      ),
      'magazine' => array(
          'title' => variable_get('magazine_landing_page_title', 'Miami Law Magazine'),
          'description' => 'University of Miami - School of Law Magazine',
          'page callback' => 'magazine_landing',
          'page arguments' => array(1),
          'access arguments' => array('access content'),
      ),
      'magazine/%/%' => array(
          'title' => 'Magazine Quarter',
          'title callback' => 'magazine_sub_title',
          'title arguments' => array(1,2),
          'description' => 'University of Miami - School of Law Magazine Quarter',
          'page callback' => 'magazine_quarter_landing',
          'load arguments' => array(1,2),
          'page arguments' => array(1,2),
          'access arguments' => array('access content')
      ),
      'magazine/%/%/%' => array(
          'title' => 'Magazine',
          'description' => 'University of Miami - School of Law Magazine',
          'page callback' => 'magazine_page',
          'load arguments' => array(1,2,3),
          'page arguments' => array(1,2,3),
          'access arguments' => array('access content')
      ),
      'magazine/%' => array(
          'title' => 'Magazine Year Landing',
          'title callback' => 'magazine_sub_title',
          'title arguments' => array(1),
          'description' => 'University of Miami - School of Law Magazine Year Landing',
          'page callback' => 'magazine_year_landing',
          'page arguments' => array(1),
          'access arguments' => array('access content'),
      ),
  );
  return $items;
}

function miamilaw_magazine_create_social_form(&$form, $name){
    $cap = lcfirst($name);

    $form['magazine-social']['magazine-' . $name . '-title'] = array(
        '#type' => 'textfield',
        '#title' => t($cap . ' Page Name'),
        '#default_value' => variable_get('magazine-' . $name . '-title', ''),
        '#description' => t('Title of this social link and image.'),
        '#required' => false,
    );

    $form['magazine-social']['magazine-' . $name . '-url'] = array(
        '#type' => 'textfield',
        '#title' => t($cap . ' Page URL'),
        '#default_value' => variable_get('magazine-' . $name . '-url', ''),
        '#description' => t($cap . ' page URL to link to.'),
        '#required' => false,
    );

    $form['magazine-social']['magazine-' . $name . '-image'] = MiamiLawTools::createFormImageField($cap . ' Page Image', $cap . ' image/logo', 'magazine-' . $name . '-image');
}

/**
 * page callback
 */
function miamilaw_magazine_form($form, &$form_state){
    if (user_access('magazine admin')){
        $form['magazine'] = array(
          '#type' => 'fieldset',
          '#title' => t('Miami Law Magazine Settings'),
          '#collapsible' => true,
          '#collapsed' => false,
        );
        $form['magazine']['magazine_landing_page_title'] = array(
            '#type' => 'textfield',
            '#title' => t('Title for the Magazine landing page (/magazine)'),
            '#default_value' => variable_get('magazine_landing_page_title', 'Miami Law Magazine'),
            '#description' => t('Set title to display.'),
            '#required' => true,
        );
        $form['magazine']['magazine_menu_title'] = array(
            '#type' => 'textfield',
            '#title' => t('Menu title for the Magazine pages (/magazine*/*)'),
            '#default_value' => variable_get('magazine_menu_title', 'CURRENT MAGAZINE'),
            '#description' => t('Set title to display at the top of the menu.'),
            '#required' => true,
        );
        $form['magazine']['magazine_feature_story_title'] = array(
            '#type' => 'textfield',
            '#title' => t('Feature Story Title'),
            '#default_value' => variable_get('magazine_feature_story_title', 'Feature Story'),
            '#description' => t('Set title to display above the feature story.'),
            '#required' => true,
        );
        $form['magazine']['magazine_print_version_title'] = array(
            '#type' => 'textfield',
            '#title' => t('Printed Version Title'),
            '#default_value' => variable_get('magazine_print_version_title', 'Explore the printed version'),
            '#description' => t('Set title to display above the printed version.'),
            '#required' => true,
        );
        $form['magazine']['magazine_social_buttons_title'] = array(
            '#type' => 'textfield',
            '#title' => t('Social Buttons Title'),
            '#default_value' => variable_get('magazine_social_buttons_title', 'Connect with us'),
            '#description' => t('Set title to display above the social buttons.'),
            '#required' => true,
        );
        $form['magazine']['magazine_additional_title'] = array(
            '#type' => 'textfield',
            '#title' => t('Additional Title'),
            '#default_value' => variable_get('magazine_additional_title', 'Also in this issue'),
            '#description' => t('Set title to display above the additional content at the bottom of the page (Also in this issue).'),
            '#required' => true,
        );

        $form['magazine-social'] = array(
            '#type' => 'fieldset',
            '#title' => t('Magazine Social Buttons'),
            '#collapsible' => true,
            '#collapsed' => true,
        );

        for($i = 0; $i < 6; $i++) {
            miamilaw_magazine_create_social_form($form, 'social-' . strval($i));
        }

        $form = system_settings_form($form);
        return $form;
    }

  return system_settings_form($form);
}

function get_magazine_list($order = 'ASC'){
    $query = new EntityFieldQuery;
    $result = $query->entityCondition('entity_type', 'node')
        ->propertyCondition('type', array('miamilaw_magazine'), 'IN')
        ->propertyCondition('status', 1)
        ->fieldOrderBy('field_magazine_year', 'value', $order)
        ->fieldOrderBy('field_magazine_quarter', 'value', $order)
        ->execute();

    if (!isset($result['node'])) {
        return array();
    }

    $nodes = node_load_multiple(array_keys($result['node']));

    $list = array();

    foreach ($nodes as $node){
        $year = explode('-', $node->field_magazine_year[LANGUAGE_NONE][0]['value'])[0];
        $quarter = magazine_extract_quarter($node);
        $list[$year][$quarter][] = $node;
    }

    return $list;
}

function get_magazine_sub_stories($year, $quarter_key){
    $query = new EntityFieldQuery;
    $result = $query->entityCondition('entity_type', 'node')
        ->propertyCondition('type', array('miamilaw_magazine_sub_story'), 'IN')
        ->propertyCondition('status', 1)
        ->fieldCondition('field_magazine_year', 'value', date($year . "-01-01 00:00:00"), '=') // Year
        ->fieldCondition('field_magazine_quarter', 'value', $quarter_key, '=') // Quarter
        ->fieldOrderBy('field_magazine_position_weight', 'value', 'ASC')
        ->execute();

    if (!isset($result['node'])) {
        return array();
    }

    $nodes = node_load_multiple(array_keys($result['node']));

    $list = array();

    foreach ($nodes as $node){
        $tags = $node->field_headlines_tags[LANGUAGE_NONE];
        $tag_list = <<<EOT
            <div class="tags">
                <ul>
EOT;
        if ($tags) {
            foreach ($tags as $tag){
                $term = taxonomy_term_load($tag['tid']);
                $uri = taxonomy_term_uri($term);
                $tag_list .= '<a href="' . file_create_url(drupal_get_path_alias($uri['path'])) . '"><li>' . $term->name . '</li></a>';
            }
        }
        $tag_list .= <<< EOT
                </ul>
            </div>
EOT;
        $list[] = array(
            'title' => $node->title,
            'url' => url($node->field_magazine_story_link[LANGUAGE_NONE][0]['url']),
            'text' => $node->field_magazine_story_text[LANGUAGE_NONE][0]['value'],
            'image' => file_create_url($node->field_magazine_story_image[LANGUAGE_NONE][0]['uri']),
            'tags' => $tag_list,
        );
    }

    return $list;
}

function get_magazine_extra_content($year, $quarter_key){
    $query = new EntityFieldQuery;
    $result = $query->entityCondition('entity_type', 'node')
        ->propertyCondition('type', array('miamilaw_magazine_extra_content'), 'IN')
        ->propertyCondition('status', 1)
        ->fieldCondition('field_magazine_year', 'value', date($year . "-01-01 00:00:00"), '=') // Year
        ->fieldCondition('field_magazine_quarter', 'value', $quarter_key, '=') // Quarter
        ->fieldOrderBy('field_magazine_position_weight', 'value', 'ASC')
        ->execute();

    if (!isset($result['node'])) {
        return array();
    }

    $nodes = node_load_multiple(array_keys($result['node']));

    $list = array();

    foreach ($nodes as $node){
        $list[] = array(
            'title' => $node->title,
            'url' => url($node->field_magazine_story_link[LANGUAGE_NONE][0]['url']),
            'text' => $node->field_magazine_story_text[LANGUAGE_NONE][0]['value'],
            'image' => file_create_url($node->field_magazine_story_image[LANGUAGE_NONE][0]['uri']),
        );
    }

    return $list;
}

function magazine_create_menu(){
    $list = get_magazine_list('DESC');
    $origin = url('magazine/');
    $current = explode('/', request_path());

    $landing_active = count($current) == 1 ? 'active-trail' : '';
    $menu_title = variable_get('magazine_menu_title', 'CURRENT MAGAZINE');
    $content = <<<EOT
        <ul class="menu">
            <li class="leaf $landing_active">
                <a href="$origin">$menu_title</a>
            </li>
EOT;

    foreach($list as $year_key => $year){
        $active = '';
        if (count($current) > 1 && $current[1] == $year_key){
            $active = 'active-trail';
        }
        $content .= '<li class="leaf ' . $active . '"><a href="' . url('magazine/' . $year_key) . '">' . $year_key . '</a>';
        if (!empty($active)){
            $content .= '<ul class="menu expanded">';
            foreach($year as $quarter_key => $quarter){
                $content .= '<li class="leaf"><a href="' . url('magazine/' . $year_key . '/' . $quarter_key) . '">' . ucfirst($quarter_key) . '</a>';
            }
            $content .= '</ul>';
        }
        $content .= '</li>';
    }

    $content .= '</li></ul>';

    return $content;
}

function magazine_landing(){
    if (!magazine_get_node()){
        return <<<EOT
        <div class="ui message">
            <div class="header">
                Notice
            </div>
            <p>No magazines have been published.</p>
        </div>
EOT;
    }
    return '';
}

function magazine_verify_year($year){
    return preg_match('/^[1-9]\d{3}$/i', $year);
}

function magazine_verify_quarter($quarter){
    $quarters = '/^([S|s]pring|[S|s]ummer|[F|f]all|[W|w]inter)$/i';
    return preg_match($quarters, $quarter);
}

function get_magazine_link($node){
    $link = '<li><a href="' . url(drupal_get_path_alias('node/' . $node->nid)) . '">' . $node->title . '</a></li>';

    return $link;
}

function magazine_get_node(){
    $node = array();
    if (arg(0) == 'node' && is_numeric(arg(1))) {
        $node = MiamiLawTools::nodeLoad(arg(1));
    } else {
        $query = new EntityFieldQuery;
        $result = $query->entityCondition('entity_type', 'node')
            ->propertyCondition('type', array('miamilaw_magazine'), 'IN')
            ->propertyCondition('status', 1)
            ->fieldOrderBy('field_magazine_year', 'value', 'ASC')
            ->fieldOrderBy('field_magazine_quarter', 'value', 'ASC')
            ->execute();
        if (isset($result['node'])) {
            $node = array_pop(node_load_multiple(array_keys($result['node'])));
        }
    }
    return $node;
}

function magazine_is_year_landing(){
    return magazine_verify_year(array_pop(explode('/', request_path())));
}

function magazine_year_landing($arg1){
    $list = get_magazine_list('DESC');
    if (magazine_verify_year($arg1) && array_key_exists($arg1, $list)) {
        //$quarters = '<div class="ui divided items">';
        $quarters = '<div class="magazine-year-landing"><div class="ui two stackable cards">';
        foreach($list[$arg1] as $key => $quarter){
            //echo "<pre>" . print_r($quarter) . "</pre>";
            $values = magazine_extract_node_contents($quarter[0]);
            $sub_stories = get_magazine_sub_stories($values['year'], $values['quarter-key']);
            $quarter = ucfirst($key);
            $url = url('magazine/' . $arg1 . '/' . $key);
            $feature = variable_get('magazine_feature_story_title', 'Feature Story');
            $also = variable_get('magazine_additional_title', 'Also in this issue');
            $sub_content = '';
            if ($sub_stories) {
                $count = 0;
                $sub_content = <<<EOT
                    <div class="content">
                        <a class="ui sub header" href="$url">$also</a>
                        <div class="ui mini divided list">
EOT;
                foreach ($sub_stories as $story) {
                    $sub_content .= <<<EOT
                        <a class="item" href="$url">
                            <img class="ui avatar image" src="{$story['image']}">
                            <div class="content">
                                <div class="description">{$story['title']}</div>
                            </div>
                        </a>
EOT;
                    if (++$count == 3){
                        break;
                    }
                }
                $sub_content .= <<<EOT
                    </div>
                </div>
EOT;
            }
            $quarters .= <<<EOT
            <div class="card">
                <div class="content">
                    <a class="header" href="$url">MIAMILAW $quarter $arg1</a>
                </div>
                <a class="image" href="$url">
                    <img src="{$values['image']}">
                </a>
                <div class="content">
                    <a class="ui sub header" href="$url">$feature</a>
                    <a class="description" href="$url">
                        {$values['title']}
                    </a>
                </div>
                $sub_content
            </div>
EOT;
        }
        $quarters .= '</div></div>';
        return $quarters;
    } else {
        return drupal_not_found();
    }
}

function magazine_quarter_landing($arg1, $arg2){
    $list = get_magazine_list('DESC');
    if (!magazine_verify_year($arg1) || !magazine_verify_quarter($arg2)) {
        return drupal_not_found();
    } else {
        if (array_key_exists($arg1, $list) && array_key_exists($arg2, $list[$arg1])){
            $quarter_name = ucfirst($arg2);
            return '';
        } else {
            return drupal_not_found();
        }
        return '';
    }
}


function magazine_page($arg1, $arg2, $arg3){
    if ($url = drupal_lookup_path("magazine/" . $arg1 . "/" . $arg2 . "/" . $arg3)){
        drupal_goto($url);
    } else {
        return drupal_not_found();
    }
}
function magazine_sub_title($arg1=null, $arg2=null){
    $title = drupal_get_title();
    if (arg(0) == 'node' && is_numeric(arg(1))) {
        if ($node = MiamiLawTools::nodeLoad(arg(1))) {
            drupal_set_title($node->title);
        }
    } else if (magazine_verify_quarter($arg2)){
        if (!preg_match('/^.*' . $arg2 . '.*$/i', $title)) {
            drupal_set_title(ucfirst($arg2) . ' ' . $arg1 . ' Magazines');
        }
    } else if (magazine_verify_year($arg1) && $arg2 == null) {
        if (!preg_match('/^.*' . $arg1 . '.*$/i', $title)) {
            drupal_set_title($arg1 . ' Magazines');
        }
    }
}

/**
 * Tokens
 */
function miamilaw_magazine_token_info(){
    return array(
        'tokens' => array(
            'node' => array(
                'magazinequarter' => array(
                    'name' => t('Magazine Quarter'),
                    'description' => t('Token to get current quarter name')
                )
            )
        )
    );
}

function miamilaw_magazine_tokens($type, $tokens, array $data = array(), array $options = array()){
    $replacements = array();
    if ($type == 'node' && !empty($data['node'])) {
        $node = $data['node'];
        foreach ($tokens as $name => $original) {
            switch ($name) {
                case 'magazinequarter':
                    $replacements[$original] = magazine_extract_quarter($node);
                    break;
            }
        }
    }
    return $replacements;
}

/**
 *
 */
function miamilaw_magazine_init(){
    if (!user_access('magazine admin')) {
    }
}
