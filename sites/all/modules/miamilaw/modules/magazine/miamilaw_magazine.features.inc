<?php
/**
 * @file
 * miamilaw_magazine_nodes.features.inc
 */

/**
 * Implements hook_node_info().
 */
function miamilaw_magazine_node_info() {
  $items = array(
    'miamilaw_magazine' => array(
      'name' => t('Magazine'),
      'base' => 'node_content',
      'description' => t('Miami Law Magazine Content Type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'miamilaw_magazine_extra_content' => array(
      'name' => t('Magazine Extra Content'),
      'base' => 'node_content',
      'description' => t('This is the additional content that is shown on the third section of the magazine.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'miamilaw_magazine_sub_story' => array(
      'name' => t('Magazine Sub-Story'),
      'base' => 'node_content',
      'description' => t('These are sub-articles and stories that will go under the Feature story of a magazine.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
