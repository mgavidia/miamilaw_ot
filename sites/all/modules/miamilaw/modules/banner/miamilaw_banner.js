/**
 * Created by mgavidia on 5/30/14.
 */
(function ($) {
    Drupal.behaviors.miamilaw_banner = {
        attach: function (context, settings) {
            var shuffler = function(array){
                var currentIndex = array.length
                    , temporaryValue
                    , randomIndex
                    ;

                // While there remain elements to shuffle...
                while (0 !== currentIndex) {

                    // Pick a remaining element...
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;

                    // And swap it with the current element.
                    temporaryValue = array[currentIndex];
                    array[currentIndex] = array[randomIndex];
                    array[randomIndex] = temporaryValue;
                }

                return array;
            };
            $('.header-background-image').once('banner-image-rotator', function(){
                var index = 0;
                var rotation = Drupal.settings.miamilaw_banner.rotate_speed;
                var fadeout = Drupal.settings.miamilaw_banner.fadeout_speed;
                var fadein = Drupal.settings.miamilaw_banner.fadein_speed;
                var images = shuffler(Drupal.settings.miamilaw_banner.urls);
                // Sets the next image
                var setImage = function(){
                    //console.log('Setting: ' + images[index]);
                    $('.header-background-image').css('background-image', 'url(' + images[index] +')');
                    //$('.header-background-image').css('top', -$(window).width()/3);
                };
                // Fade in function
                var fadein_func = function(){
                    $this = $(this);
                    index = (index + 1) % images.length;
                };

                // Set initial image and fade in
                setImage();
                $('.header-background-image').fadeOut(0, function() {
                    $this = $(this);
                    $this.fadeIn(fadein, fadein_func);
                });

                var t = setInterval(function(){
                    $('.header-background-image').fadeOut(fadeout, function(){
                        $this = $(this);
                        setImage();
                        $this.fadeIn(fadein, fadein_func);
                    });
                }, rotation);

                $(window).resize(function(){
                    //$('.header-background-image').css('top', -$(window).width()/3);
                });

            });
        }
    };
})(jQuery);