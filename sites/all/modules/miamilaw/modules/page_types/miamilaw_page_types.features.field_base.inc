<?php
/**
 * @file
 * miamilaw_page_types.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function miamilaw_page_types_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_associated_menu'
  $field_bases['field_associated_menu'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_associated_menu',
    'foreign keys' => array(),
    'indexes' => array(
      'menureference' => array(
        0 => 'menureference',
      ),
    ),
    'locked' => 0,
    'module' => 'menufield',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'menufield_menureference',
  );

  // Exported field_base: 'field_page_body'
  $field_bases['field_page_body'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_page_body',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_section_content'
  $field_bases['field_section_content'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_section_content',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  return $field_bases;
}
