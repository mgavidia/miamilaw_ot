<?php
/**
 * @file
 * miamilaw_page_types.features.inc
 */

/**
 * Implements hook_node_info().
 */
function miamilaw_page_types_node_info() {
  $items = array(
    'miamilaw_page' => array(
      'name' => t('Basic Webpage'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'miamilaw_section' => array(
      'name' => t('Section Homepage'),
      'base' => 'node_content',
      'description' => t('Use <em>section pages</em> for your section landing pages.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
