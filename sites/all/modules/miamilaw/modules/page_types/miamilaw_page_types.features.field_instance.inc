<?php
/**
 * @file
 * miamilaw_page_types.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function miamilaw_page_types_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-miamilaw_page-field_associated_menu'
  $field_instances['node-miamilaw_page-field_associated_menu'] = array(
    'bundle' => 'miamilaw_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select the custom menu that this webpage is associated with.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_associated_menu',
    'label' => 'Basic Webpage Navigation Menu',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'menufield',
      'settings' => array(),
      'type' => 'menufield_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-miamilaw_page-field_headlines_page_default'
  $field_instances['node-miamilaw_page-field_headlines_page_default'] = array(
    'bundle' => 'miamilaw_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Filter the headlines row with the specific relevant content tag associated with this content. 
Otherwise select None to omit the headlines row.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 12,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_headlines_page_default',
    'label' => 'Headlines Row Filter Tag',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-miamilaw_page-field_page_body'
  $field_instances['node-miamilaw_page-field_page_body'] = array(
    'bundle' => 'miamilaw_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_page_body',
    'label' => 'Page Content',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 15,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-miamilaw_page-field_right_rail_reference'
  $field_instances['node-miamilaw_page-field_right_rail_reference'] = array(
    'bundle' => 'miamilaw_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 10,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_right_rail_reference',
    'label' => 'Right Rail',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-miamilaw_page-field_section'
  $field_instances['node-miamilaw_page-field_section'] = array(
    'bundle' => 'miamilaw_page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_section',
    'label' => 'Section',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-miamilaw_section-field_headlines_page_default'
  $field_instances['node-miamilaw_section-field_headlines_page_default'] = array(
    'bundle' => 'miamilaw_section',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Filter the headlines row with the specific relevant content tag associated with this content. 
Otherwise select None to omit the headlines row.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_headlines_page_default',
    'label' => 'Headlines Row Filter Tag',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-miamilaw_section-field_section'
  $field_instances['node-miamilaw_section-field_section'] = array(
    'bundle' => 'miamilaw_section',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_section',
    'label' => 'Section',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-miamilaw_section-field_section_content'
  $field_instances['node-miamilaw_section-field_section_content'] = array(
    'bundle' => 'miamilaw_section',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_section_content',
    'label' => 'Page Content',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Basic Webpage Navigation Menu');
  t('Filter the headlines row with the specific relevant content tag associated with this content. 
Otherwise select None to omit the headlines row.');
  t('Headlines Row Filter Tag');
  t('Page Content');
  t('Right Rail');
  t('Section');
  t('Select the custom menu that this webpage is associated with.');

  return $field_instances;
}
