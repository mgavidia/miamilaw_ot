/**
 * Created by mgavidia on 8/21/14.
 */
(function($) {

    Drupal.behaviors.jwplayer = {
        attach: function(context, settings){
            var content = $('p').find('jwplayer');
            if (!content.length){
                //console.log('not found.');
                return;
            }

            content.each(function() {
                var player = $(this);
                if (player.attr('data-file')) {
                    var server = Drupal.settings.miamilaw_jwplayer.stream_server;
                    var image = player.attr('data-image') ? player.attr('data-image') : Drupal.settings.miamilaw_jwplayer.video_image;
                    var width = player.attr('data-width') ? player.attr('data-width') : '100%';
                    var aspectratio = player.attr('data-aspectratio') ? player.attr('data-aspectratio') : '16:9';
                    var auto = player.attr('data-autostart') == '1';
                    jwplayer(player.attr('data-id')).setup({
                        file: player.attr('data-file'),
                        image: image,
                        width: width,
                        aspectratio: aspectratio,
                        title: player.attr('data-title'),
                        /*playlist: [{
                            image: image,
                            sources: [{
                                file: 'rtmp://' + server + '/vod/' + player.attr('data-file')
                            }, {
                                file: 'http://' + server + ':1935/vod/' + player.attr('data-file') + "/playlist.m3u8"
                            }]
                        }],*/
                        sources: [{
                            file: 'http://' + server + ':1935/vod/' + player.attr('data-file') + "/playlist.m3u8"
                        }, {
                            file: 'rtmp://' + server + '/vod/' + player.attr('data-file')
                        }],
                        primary: "html5",
                        autostart: auto

                    });
                }
            });
        }
    };
})(jQuery);
