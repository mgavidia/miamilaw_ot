/**
 * @file Plugin for adding button to ckeditor for jwplayer
 */
(function($) {

    CKEDITOR.plugins.add('jwplayer',
        {
            requires: ['dialog'],
            icons: 'jwplayer',
            init: function(editor) {
                editor.addCommand('jwplayer',
                    {exec: function(editor){
                        editor.openDialog('playerDialog');
                    }}
                );
                editor.ui.addButton('jwplayer', {
                   label: 'Add JW Player to text',
                   command: 'jwplayer',
                   icon: this.path + 'icons/jwplayer.png'
                });
            }
        });

    CKEDITOR.dialog.add( 'playerDialog', function( editor )
    {
        return {
            title : 'JW Player Content',
            minWidth : 300,
            minHeight : 200,
            contents :
                [
                    {
                        id : 'general',
                        label : 'Settings',
                        elements :
                            [
                                {
                                    type : 'html',
                                    html : 'Input the details of the video.'
                                },
                                {
                                    type : 'text',
                                    id : 'title',
                                    label : 'Title of Video',
                                    validate : CKEDITOR.dialog.validate.notEmpty( 'Must have a title.' ),
                                    required : true,
                                    commit : function( data )
                                    {
                                        data.title = this.getValue();
                                    }
                                },
                                {
                                    type : 'text',
                                    id : 'uniqueid',
                                    label : 'Unique Identifier (ex. video-1 or video-2)',
                                    validate : CKEDITOR.dialog.validate.notEmpty( 'Must have a unique id and should be different for each one on the page.' ),
                                    required : true,
                                    commit : function( data )
                                    {
                                        data.uniqueid = this.getValue();
                                    }
                                },
                                {
                                    type : 'text',
                                    id : 'filename',
                                    label : 'Name of File (ex. somefile.mp4)',
                                    validate : CKEDITOR.dialog.validate.notEmpty( 'Must have a filename.' ),
                                    required : true,
                                    commit : function( data )
                                    {
                                        data.filename = this.getValue();
                                    }
                                },
                                {
                                    type : 'radio',
                                    id : 'autostart',
                                    label : 'Autostart Playback',
                                    validate : CKEDITOR.dialog.validate.notEmpty( 'Yes or no' ),
                                    required : true,
                                    items: [ ['Yes', '1'], ['No', '0']],
                                    'default': '0',
                                    commit : function( data )
                                    {
                                        data.autostart = this.getValue();
                                    }
                                },
                                {
                                    type : 'text',
                                    id : 'width',
                                    label : 'Width of Video in percentage (Defaults to 100%)',
                                    commit : function( data )
                                    {
                                        data.width = this.getValue();
                                    }
                                },
                                {
                                    type : 'text',
                                    id : 'aspectratio',
                                    label : 'Aspect Ratio of Video (Defaults to 16:9)',
                                    commit : function( data )
                                    {
                                        data.aspectratio = this.getValue();
                                    }
                                },
                                {
                                    type: 'text',
                                    id: 'image',
                                    label: 'URL to image that is shown before player starts. (Defaults to MIAMILAW logo)',
                                    commit: function(data){
                                        data.image = this.getValue();
                                    }
                                }
                            ]
                    }
                ],
            onOk : function()
            {
                var dialog = this,
                    data = {};
                this.commitContent( data );

                var videoTitle = 'data-title="' + data.title + '"';
                var videoFile = 'data-file="' + data.filename + '"';
                var videoID = 'id="' + data.uniqueid + '" data-id="' + data.uniqueid + '"';
                var videoWidth = data.width ? 'data-width="' + data.width + '"' : '';
                var videoAspectratio = data.aspectratio ? 'data-aspectratio="' + data.aspectratio + '"' : '';
                var autostart = data.autostart ? 'data-autostart="' + data.autostart + '"' : 'data-autostart="0"';
                var videoImage = data.image ? 'data-image="' + data.image + '"' : '';

                var element = CKEDITOR.dom.element.createFromHtml('<jwplayer ' + videoID + ' ' + videoTitle + ' ' + videoFile + ' ' + autostart + ' ' + videoWidth + ' ' + videoAspectratio +  ' ' + videoImage + '>Embedded Video</jwplayer>');
                //editor.insertHtml( '<jwplayer id="jwplayer" ' + videoTitle + ' ' + videoFile + ' ' + autostart + ' ' + videoWidth + ' ' + videoHeight + '>Embedded Video</jwplayer>');
                editor.insertElement(element);
            }
        };
    });

})(jQuery);
