/**
 * Created by mgavidia on 8/21/14.
 */
(function($) {

    function scrollToAnchor(elem){
        //var aTag = $("div[id='"+ id +"']");
        $('html,body').animate({scrollTop: elem.offset().top},'slow');
    }

    function getUrlParameter(sParam)
    {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam)
            {
                return sParameterName[1];
            }
        }
    }

    Drupal.behaviors.miamilaw_collapse = {
        attach: function (context, settings) {
            var collapsible = $('.collapsible-content');
            if (!collapsible.length){
                return;
            }

            collapsible.find('.collapse-action').each(function () {
                var content = $(this);
                var toggled = true;
                var title = content.text();
                content.next().toggle();
                content.click(function (event) {
                    event.preventDefault();
                    content.next().slideToggle({
                        duration: 300,
                        easing: 'swing',
                        complete: function () {
                            toggled = !toggled;
                            if (toggled) {
                                content.html('<h3><i class="fa fa-chevron-right"></i>' + title + '</h3>');
                            } else {
                                content.html('<h3><i class="fa fa-chevron-down"></i>' + title + '</h3>');
                            }
                        }
                    });
                })
            });

            /*var url = window.location.hash;
            var hash = url.substring(url.indexOf("#")+1);*/
            var hash = getUrlParameter('anchor');
            //console.log('hash: ' + hash);
            if (hash && hash.length > 0) {
                collapsible.each(function () {
                    var item = $(this);
                    //console.log('Checking element id: ' + item.attr('id'));
                    if (item.attr('id') == hash) {
                        var content = $('.collapse-action', item);//.find('.collapse-action');
                        content.trigger('click');
                        scrollToAnchor(item);
                        return false;
                    }
                });
            }
        }
    }
})(jQuery);
