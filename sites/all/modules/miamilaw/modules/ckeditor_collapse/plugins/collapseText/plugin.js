/**
 * @file Plugin for adding button to ckeditor for collapse text
 */
(function($) {

    var currentValue = '';

    CKEDITOR.plugins.add('collapseText',
        {
            requires: ['dialog'],
            icons: 'collapseText',
            init: function(editor) {
                editor.addCommand('wrapCollapse',
                    {exec: function(editor){
                        var mySelection = editor.getSelection();
                        var selectedText = '';
                        if (CKEDITOR.env.ie) {
                            mySelection.unlock(true);
                            selectedText = mySelection.getNative().createRange().text;
                        } else {
                            selectedText = mySelection.getNative();
                        }
                        if (selectedText){
                            //editor.insertHtml('<p>[collapsible title=""]</p>' + selectedText + '<p>[/collapsible]</p>');
                            currentValue = selectedText;
                            editor.openDialog('wrapDialog');
                        }
                    }}
                );
                editor.ui.addButton('collapse', {
                   label: 'Make selected text collapsible',
                   command: 'wrapCollapse',
                   icon: this.path + 'icons/select.png'
                });
            }
        });

    CKEDITOR.dialog.add( 'wrapDialog', function( editor )
    {
        return {
            title : 'Collapsed Content',
            minWidth : 300,
            minHeight : 200,
            contents :
                [
                    {
                        id : 'general',
                        label : 'Settings',
                        elements :
                            [
                                {
                                    type : 'html',
                                    html : 'Input the title of the collapsed content.'
                                },
                                {
                                    type : 'text',
                                    id : 'title',
                                    label : 'TITLE',
                                    validate : CKEDITOR.dialog.validate.notEmpty( 'The wrapped content must have a title.' ),
                                    required : true,
                                    commit : function( data )
                                    {
                                        data.title = this.getValue();
                                    }
                                }
                            ]
                    }
                ],
            onOk : function()
            {
                if (currentValue){
                    var dialog = this,
                        data = {};
                    this.commitContent( data );
                    editor.insertHtml( '<p>[collapsible title="' + data.title + '"]</p>' + currentValue + '<p>[/collapsible]</p>' );
                    currentValue = '';
                }
            }
        };
    });

})(jQuery);
