(function ($) {
    Drupal.behaviors.miamilaw_calendar_events = {
        attach: function (context, settings) {
            var events = $("[class^=l-events]");
            if (!events.length){
                return;
            }

            $(window).bind('load', function() {
                events.once('setup-events', function () {

                    var setupMasonry = function() {
                        var container = $('.events', events);
                        try {
                            container.masonry({
                                itemSelector: '.event',
                                isAnimated: false
                            });
                        } catch (err) {
                        }

                        var controls = events.find('.controls').first();
                        var previous = $('#events-previous', controls);
                        var next = $('#events-next', controls);


                        previous.click(function(){
                            //console.log("Previous");
                        });
                        next.click(function(){
                            //console.log("Next");
                        });
                    };

                    var destroyMasonry = function() {
                        var sleep = function sleep(milliseconds) {
                            var start = new Date().getTime();
                            for (var i = 0; i < 1e7; i++) {
                                if ((new Date().getTime() - start) > milliseconds){
                                    break;
                                }
                            }
                        };
                        var container = $('.events', events);
                        try {
                            container.children('.event').each(function(){
                                //console.log(this);
                                container.masonry('remove', $(this));
                            });
                            /*container.append('<div class="event"><h3 class="date"></h3></div>');
                            container.append('<div class="event"><h3 class="date">Loading events.</h3></div>');*/
                            container.append('<div class="loading" style="padding-top: 6.5em"><i class="notched circle loading icon"></i> Loading Events</div>');
                        } catch (err) {

                        }
                    };

                    setupMasonry();

                    var dropdown = events.find('.drop-down').first();
                    var list = events.find('.links').first();

                    events.ajaxSend(function(event, xhr, settings){
                        if(settings.url.indexOf('/ajax/events/reload') > -1) {
                            destroyMasonry();
                        }
                    });

                    events.ajaxComplete(function(event, xhr, settings) {
                        if(settings.url.indexOf('/ajax/events/reload') > -1){
                            setupMasonry();
                        }
                    });
                });
            });
        }
    }
})(jQuery);