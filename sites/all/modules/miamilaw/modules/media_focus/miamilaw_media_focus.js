/**
 * Created by mgavidia on 8/19/14.
 */

(function ($) {
    Drupal.behaviors.miamilaw_media_focus = {
        attach: function (context, settings) {
            var right = $("[class^=l-media]").find('.right').first();
            if (!right.length){
                return;
            }

            $(window).bind('load', function() {
                right.once('right-overlay-fix', function () {

                    var grid = $('.grid', right);

                    /*grid.sliphover({
                     target: 'img',
                     duration: 250,
                     height: '100%',
                     reverse: false,
                     enterCallback: function(element){
                     element.fadeOut();
                     },
                     exitCallback: function(element){
                     element.fadeIn();//css('display', 'block');
                     }
                     });*/

                    grid.masonry({
                        itemSelector: 'a'
                    });
                });
            });

        }
    }
})(jQuery);