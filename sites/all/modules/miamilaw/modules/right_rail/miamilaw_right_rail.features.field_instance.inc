<?php
/**
 * @file
 * miamilaw_right_rail.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function miamilaw_right_rail_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-miamilaw_right_rail-field_right_rail_content'
  $field_instances['node-miamilaw_right_rail-field_right_rail_content'] = array(
    'bundle' => 'miamilaw_right_rail',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Content to display for the Right Rail',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div',
    'field_name' => 'field_right_rail_content',
    'label' => 'Content',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Content to display for the Right Rail');

  return $field_instances;
}
