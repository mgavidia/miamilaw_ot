<?php
/**
 * @file
 * miamilaw_right_rail.features.inc
 */

/**
 * Implements hook_node_info().
 */
function miamilaw_right_rail_node_info() {
  $items = array(
    'miamilaw_right_rail' => array(
      'name' => t('Basic Webpage Right Rail'),
      'base' => 'node_content',
      'description' => t('Right Rail Content'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
