<?php
/**
 * @file
 * miamilaw_headlines.features.inc
 */

/**
 * Implements hook_node_info().
 */
function miamilaw_headlines_node_info() {
  $items = array(
    'miami_law_article' => array(
      'name' => t('News Article'),
      'base' => 'node_content',
      'description' => t('Miami Law Article Content Type'),
      'has_title' => '1',
      'title_label' => t('Headline'),
      'help' => '',
    ),
    'miami_law_document' => array(
      'name' => t('Headlines Row Link to Document'),
      'base' => 'node_content',
      'description' => t('Miami Law Document Content'),
      'has_title' => '1',
      'title_label' => t('Headline'),
      'help' => '',
    ),
    'miami_law_external_link' => array(
      'name' => t('Headlines Row External Link'),
      'base' => 'node_content',
      'description' => t('Miami Law External Links'),
      'has_title' => '1',
      'title_label' => t('Headline'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
