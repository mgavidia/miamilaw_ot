<?php

/***
 * Provides the necessary fields to use
 */

class HeadlinesData {
    public static $image = 'field_headlines_image';
    public static $text = 'field_headlines_text';
    public static $link = 'field_headlines_link';
    public static $linkType = 'field_article_link_icon';
    public static $tags = 'field_headlines_tags';

    private static function deleteFields(){
        $fields = array();

        $fields[HeadlinesData::$image] = true;
        $fields[HeadlinesData::$text] = true;
        $fields[HeadlinesData::$link] = true;
        $fields[HeadlinesData::$linkType] = true;
        $fields[HeadlinesData::$tags] = false;

        return $fields;
    }

    public static function getFieldNames(){
        return array_keys(HeadlinesData::deleteFields());
    }

    public static function getDeleteFields($name) {
        $fields = HeadlinesData::deleteFields();
        return $fields[$name];
    }

    public static function getFieldInstances($name, $bundle){
        $instances = array();

        $instances[HeadlinesData::$image] = array(
            'bundle' => $bundle,
            'deleted' => 0,
            'description' => '',
            'display' => array(
                'default' => array(
                    'label' => 'above',
                    'module' => 'image',
                    'settings' => array(
                        'image_link' => '',
                        'image_style' => '',
                    ),
                    'type' => 'image',
                    'weight' => 1,
                ),
            ),
            'entity_type' => 'node',
            'fences_wrapper' => 'no_wrapper',
            'field_name' => HeadlinesData::$image,
            'label' => 'Headlines Image',
            'required' => 1,
            'settings' => array(
                'alt_field' => 0,
                'default_image' => 0,
                'file_directory' => '',
                'file_extensions' => 'png gif jpg jpeg',
                'max_filesize' => '',
                'max_resolution' => '',
                'min_resolution' => '',
                'title_field' => 0,
                'user_register_form' => FALSE,
            ),
            'widget' => array(
                'active' => 1,
                'module' => 'media',
                'settings' => array(
                    'allowed_schemes' => array(
                        0 => 'public',
                        1 => 'private',
                    ),
                    'allowed_types' => array(
                        0 => 'image',
                    ),
                    'preview_image_style' => 'thumbnail',
                    'progress_indicator' => 'throbber',
                ),
                'type' => 'media_generic',
                'weight' => 2,
            ),
        );

        // Exported field_instance: 'node-delete_me-field_headlines_text'
        $instances[HeadlinesData::$text] = array(
            'bundle' => $bundle,
            'default_value' => NULL,
            'deleted' => 0,
            'description' => 'Headlines Text.',
            'display' => array(
                'default' => array(
                    'label' => 'above',
                    'module' => 'text',
                    'settings' => array(),
                    'type' => 'text_default',
                    'weight' => 0,
                ),
                'teaser' => array(
                    'label' => 'above',
                    'settings' => array(),
                    'type' => 'hidden',
                    'weight' => 0,
                ),
            ),
            'entity_type' => 'node',
            'fences_wrapper' => 'no_wrapper',
            'field_name' => HeadlinesData::$text,
            'label' => 'Headlines Text',
            'required' => 1,
            'settings' => array(
                'text_processing' => 0,
                'user_register_form' => FALSE,
            ),
            'widget' => array(
                'active' => 1,
                'module' => 'text',
                'settings' => array(
                    'rows' => 5,
                ),
                'type' => 'text_textarea',
                'weight' => 1,
            ),
        );

        $instances[HeadlinesData::$link] = array(
            'bundle' => $bundle,
            'default_value' => NULL,
            'deleted' => 0,
            'description' => '',
            'display' => array(
                'default' => array(
                    'label' => 'above',
                    'module' => 'link',
                    'settings' => array(),
                    'type' => 'link_default',
                    'weight' => 2,
                ),
            ),
            'entity_type' => 'node',
            'fences_wrapper' => 'no_wrapper',
            'field_name' => HeadlinesData::$link,
            'label' => 'Headlines Link',
            'required' => 1,
            'settings' => array(
                'attributes' => array(
                    'class' => '',
                    'configurable_title' => 0,
                    'rel' => '',
                    'target' => 'default',
                    'title' => '',
                ),
                'display' => array(
                    'url_cutoff' => 80,
                ),
                'enable_tokens' => 1,
                'rel_remove' => 'default',
                'title' => 'none',
                'title_maxlength' => 128,
                'title_value' => '',
                'url' => 0,
                'user_register_form' => FALSE,
                'validate_url' => 1,
            ),
            'widget' => array(
                'active' => 0,
                'module' => 'link',
                'settings' => array(),
                'type' => 'link_field',
                'weight' => 3,
            ),
        );

        // Exported field_instance: HeadlinesData::$linkType
        $instances[HeadlinesData::$linkType] = array(
            'bundle' => $bundle,
            'default_value' => NULL,
            'deleted' => 0,
            'description' => '',
            'display' => array(
                'default' => array(
                    'label' => 'above',
                    'module' => 'list',
                    'settings' => array(),
                    'type' => 'list_default',
                    'weight' => 1,
                ),
                'teaser' => array(
                    'label' => 'above',
                    'settings' => array(),
                    'type' => 'hidden',
                    'weight' => 0,
                ),
            ),
            'entity_type' => 'node',
            'fences_wrapper' => 'no_wrapper',
            'field_name' => HeadlinesData::$linkType,
            'label' => 'Headlines Link Type',
            'required' => 1,
            'settings' => array(
                'user_register_form' => FALSE,
            ),
            'widget' => array(
                'active' => 1,
                'module' => 'options',
                'settings' => array(),
                'type' => 'options_select',
                'weight' => 2,
            ),
        );

        $instances[HeadlinesData::$tags] = array(
            'bundle' => $bundle,
            'default_value' => NULL,
            'deleted' => 0,
            'description' => 'Headlines Tags',
            'display' => array(
                'default' => array(
                    'label' => 'above',
                    'module' => 'taxonomy',
                    'settings' => array(),
                    'type' => 'taxonomy_term_reference_link',
                    'weight' => 0,
                ),
                'teaser' => array(
                    'label' => 'above',
                    'settings' => array(),
                    'type' => 'hidden',
                    'weight' => 0,
                ),
            ),
            'entity_type' => 'node',
            'fences_wrapper' => 'no_wrapper',
            'field_name' => HeadlinesData::$tags,
            'label' => 'Headlines Tags',
            'required' => 1,
            'settings' => array(
                'user_register_form' => FALSE,
            ),
            'widget' => array(
                'active' => 0,
                'module' => 'taxonomy',
                'settings' => array(
                    'autocomplete_path' => 'taxonomy/autocomplete',
                    'size' => 60,
                ),
                'type' => 'taxonomy_autocomplete',
                'weight' => 2,
            ),
        );

        return $instances[$name];
    }

    public static function updateEntries($values){
        foreach (array_keys(node_type_get_names()) as $name){
            if (isset($values) && in_array($name, $values, true)){
                // Add it only if field doesn't exist
                foreach (HeadlinesData::getFieldNames() as $fieldName){
                    $instance = field_info_instance('node',$fieldName,$name);
                    if (!isset($instance)){
                        $instance = HeadlinesData::getFieldInstances($fieldName, $name);
                        field_create_instance($instance);
                    }
                }
            } else {
                // Remove it if not selected
                foreach (HeadlinesData::getFieldNames() as $fieldName){
                    $instance = field_info_instance('node',$fieldName,$name);
                    if (isset($instance)){
                        field_delete_instance($instance, false);
                    }
                }
            }
        }
    }

    public static function removeAll(){
        foreach (array_keys(node_type_get_names()) as $name){
            // Remove it
            foreach (HeadlinesData::getFieldNames() as $fieldName){
                $instance = field_info_instance('node',$fieldName,$name);
                if (isset($instance)){
                    field_delete_instance($instance, HeadlinesData::getDeleteFields($fieldName));
                }
            }
        }
    }
}