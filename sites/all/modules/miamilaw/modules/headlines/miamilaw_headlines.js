/**
 * Created by mgavidia on 7/31/14.
 */
(function ($) {
    Drupal.behaviors.miamilaw_headlines = {
        attach: function (context, settings) {
            var headlines = $("[class^=l-impact]");
            if (!headlines.length){
                return;
            }

            $(window).bind('load', function() {
                headlines.once('headlines-behaviors', function () {
                    var adjustFloater = function () {
                        var container = $('.boxes', headlines);
                        if (container) {
                            try {
                                container.masonry({
                                    itemSelector: '.box, .message',
                                    isAnimated: false
                                });
                            } catch (err) {
                            }
                        }
                        var boxes = headlines.find(".box");
                        boxes.each(function () {
                            // Set floating overlay
                            var image = $('img.headlines-load', $(this));
                            var left = image.position().left;
                            var contentFloater = $('.content-type-overlay', $(this));
                            var glyph = $('i', contentFloater);
                            glyph.css({
                                'left': '-.1em',
                                'top': '.2em',
                                'padding-top': '8px'
                            });
                            contentFloater.css({
                                'left': left - (contentFloater.width() / 2)
                            });
                        });
                        if ($(window).width() < 1200) {
                            headlines.css('height', 'auto');
                        }
                    };

                    adjustFloater();
                    $(document).ajaxSend(function (event, xhr, settings) {
                        var select = $('.chosen-processed', headlines);
                        var value = select.chosen().val();
                        var filterName = $('option[value=' + value + ']', select).text();

                        if(settings.data.substring(0,9) == 'drop_down' || settings.url.indexOf('/ajax/headlines/reload') > -1){

                           var container = $('.boxes', headlines);
                           try {
                               container.children('.box').each(function(){
                                   //console.log(this);
                                   container.masonry('remove', $(this));
                               });
                               container.append('<div class="loading" style="padding-top: 6.5em"><i class="notched circle loading icon"></i> Loading ' + filterName + ' Headlines</div>');
                           } catch (err) {

                           }
                       }
                    });
                    $(document).ajaxComplete(function (event, xhr, settings) {
                        if(settings.data.substring(0,9) == 'drop_down' || settings.url.indexOf('/ajax/headlines/reload') > -1){
                            adjustFloater();
                        }
                    });
                    $(window).resize(function () {
                        adjustFloater();
                    });
                });
            });
        }
    }
})(jQuery);