/**
 * @file Plugin for adding button to ckeditor for social links
 */
(function($) {
    /** No foreach */
    if (!Array.prototype.forEach) {
        Array.prototype.forEach = function(fn, scope) {
	    for(var i = 0, len = this.length; i < len; ++i) {
	        fn.call(scope, this[i], i, this);
	    }
        }
    }
    var nextid = 0;
    var lookups = {};
    Drupal.settings.miamilaw_social.content.forEach(function(item){
        lookups[item.mlid] = item;
    });
    var createTab = function(){
        var currentid = String(nextid++);

        var setData = function(data, type, value){
            if (!data.items){
                data.items = {};
            }
            if (!data.items[currentid]){
                data.items[currentid] = {}
            }
            data.items[currentid][type] = value;
        };

        var getList = function() {
            var list = [];
            for (var key in lookups){
                var value = lookups[key];
                list.push([value.name, key]);
            }

            return list;
        };

        return {
            id : 'social-' + currentid,
            label: 'Social [' + currentid + ']',
            elements: [
                {
                    type : 'html',
                    html : 'Add social links.'
                },
                {
                    type:   'select',
                    id:     'social-icon-' + currentid,
                    label:  'Select social icon to use for this link.',
                    items:  getList(),
                    commit: function(data){
                        setData(data, 'icon', this.getValue());
                    }
                },
                {
                    type : 'text',
                    id : 'title-' + currentid,
                    label : 'Title of Social Icon',
                    //validate : CKEDITOR.dialog.validate.notEmpty( 'Must have a title.' ),
                    commit : function( data )
                    {
                        setData(data, 'title', this.getValue());
                    }
                },
                {
                    type : 'text',
                    id : 'link-' + currentid,
                    label : 'Social Website URL',
                    //validate : CKEDITOR.dialog.validate.notEmpty( 'Must have a link.' ),
                    commit : function( data )
                    {
                        setData(data, 'link', this.getValue());
                    }
                },
                {
                    type : 'select',
                    id : 'color-' + currentid,
                    label : 'Color to apply',
                    items: [
                        ['Black', 'black'],
                        ['Teal (Twitter)', 'teal'],
                        ['Red (YouTube)', 'red'],
                        ['Blue (Facebook and LinkedIn)', 'blue'],
                        ['Brown (Instagram)', 'brown'],
                        ['Pink (Flickr)', 'pink']
                    ],
                    commit : function( data )
                    {
                        setData(data, 'color', this.getValue());
                    }
                }
            ]
        };
    };

    CKEDITOR.plugins.add('social',
        {
            requires: ['dialog'],
            icons: 'social',
            init: function(editor) {
                editor.addCommand('social',
                    {exec: function(editor){
                        editor.openDialog('socialDialog');
                    }}
                );
                editor.ui.addButton('social', {
                    label: 'Add social links to text.',
                    command: 'social',
                    icon: this.path + 'icons/social.png'
                });
            }
        });

    CKEDITOR.dialog.add( 'socialDialog', function( editor )
    {
        return {
            title : 'Social Links Content',
            minWidth : 650,
            minHeight : 200,
            contents: [
                createTab(),
                createTab(),
                createTab(),
                createTab(),
                createTab(),
                createTab(),
                createTab(),
                createTab(),
                createTab()
            ],
            onOk : function()
            {
                var dialog = this,
                    data = {};
                this.commitContent( data );

                var social = '<p>[social]';
                for (var i = 0; i < 9; i++){
                    if (data.items[i].icon) {
                        social += '[social-link]' + data.items[i].icon + '|' + data.items[i].title + '|' + data.items[i].link + '|' + data.items[i].color +'[/social-link]';
                    }
                }

                social += '[/social]</p>';

                var element = CKEDITOR.dom.element.createFromHtml(social);
                editor.insertElement(element);
            }
        };
    });

})(jQuery);
