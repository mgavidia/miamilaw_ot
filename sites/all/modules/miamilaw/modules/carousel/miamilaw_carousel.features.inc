<?php
/**
 * @file
 * miamilaw_carousel.features.inc
 */

/**
 * Implements hook_node_info().
 */
function miamilaw_carousel_node_info() {
  $items = array(
    'carousel_content' => array(
      'name' => t('Carousel Content'),
      'base' => 'node_content',
      'description' => t('Carousel content type to display on specific pages by taxonomy vocabulary term.'),
      'has_title' => '1',
      'title_label' => t('Promo Headline'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
