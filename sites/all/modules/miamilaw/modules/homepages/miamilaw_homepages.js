/**
 * Created by mgavidia on 2/18/15.
 */
(function ($) {
    Drupal.behaviors.miamilaw_homepages = {
        attach: function (context, settings) {
            var iml = $('.iml-background');
            if (!iml.length){
                return;
            }

            //console.log('setting background for iml homepage.');

            iml.once('iml-set-background', function(){
                var background = Drupal.settings.miamilaw_iml_homepage.background;
                //console.log(background);
                // Sets the next image
                var setImage = function(){
                    var content = $('.l-iml');
                    content.height('auto');
                    iml.css('background-image', 'url(' + background +')');
                };

                // Set initial image and fade in
                setImage();
            });
        }
    };
})(jQuery);