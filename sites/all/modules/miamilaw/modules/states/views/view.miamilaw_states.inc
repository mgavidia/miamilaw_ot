<?php

function get_search_view() {
    $view = new view();
    $view->name = 'search_content';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'node';
    $view->human_name = 'Search Content';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = 'Content (Search)';
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'role';
    $handler->display->display_options['access']['role'] = array(
        3 => '3',
        5 => '5',
        4 => '4',
        6 => '6',
    );
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['query']['options']['distinct'] = TRUE;
    $handler->display->display_options['exposed_form']['type'] = 'input_required';
    $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '25';
    $handler->display->display_options['style_plugin'] = 'table';
    /* Relationship: Content: Author */
    $handler->display->display_options['relationships']['uid']['id'] = 'uid';
    $handler->display->display_options['relationships']['uid']['table'] = 'node';
    $handler->display->display_options['relationships']['uid']['field'] = 'uid';
    /* Field: Content: Title */
    $handler->display->display_options['fields']['title']['id'] = 'title';
    $handler->display->display_options['fields']['title']['table'] = 'node';
    $handler->display->display_options['fields']['title']['field'] = 'title';
    $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
    $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
    /* Field: Content: Section */
    $handler->display->display_options['fields']['field_section']['id'] = 'field_section';
    $handler->display->display_options['fields']['field_section']['table'] = 'field_data_field_section';
    $handler->display->display_options['fields']['field_section']['field'] = 'field_section';
    $handler->display->display_options['fields']['field_section']['empty'] = 'No Associated Section';
    /* Field: Content: Type */
    $handler->display->display_options['fields']['type']['id'] = 'type';
    $handler->display->display_options['fields']['type']['table'] = 'node';
    $handler->display->display_options['fields']['type']['field'] = 'type';
    /* Field: User: Name */
    $handler->display->display_options['fields']['name']['id'] = 'name';
    $handler->display->display_options['fields']['name']['table'] = 'users';
    $handler->display->display_options['fields']['name']['field'] = 'name';
    $handler->display->display_options['fields']['name']['relationship'] = 'uid';
    $handler->display->display_options['fields']['name']['label'] = 'Author';
    $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
    /* Field: Content: Published */
    $handler->display->display_options['fields']['status']['id'] = 'status';
    $handler->display->display_options['fields']['status']['table'] = 'node';
    $handler->display->display_options['fields']['status']['field'] = 'status';
    $handler->display->display_options['fields']['status']['type'] = 'published-notpublished';
    $handler->display->display_options['fields']['status']['not'] = 0;
    /* Field: Content: Updated date */
    $handler->display->display_options['fields']['changed']['id'] = 'changed';
    $handler->display->display_options['fields']['changed']['table'] = 'node';
    $handler->display->display_options['fields']['changed']['field'] = 'changed';
    $handler->display->display_options['fields']['changed']['label'] = 'Updated';
    $handler->display->display_options['fields']['changed']['date_format'] = 'short';
    $handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
    /* Field: Content: Edit link */
    $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
    $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
    $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
    $handler->display->display_options['fields']['edit_node']['label'] = 'Edit';
    $handler->display->display_options['fields']['edit_node']['exclude'] = TRUE;
    /* Field: Content: Delete link */
    $handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
    $handler->display->display_options['fields']['delete_node']['table'] = 'views_entity_node';
    $handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
    $handler->display->display_options['fields']['delete_node']['label'] = 'Delete';
    $handler->display->display_options['fields']['delete_node']['exclude'] = TRUE;
    $handler->display->display_options['fields']['delete_node']['alter']['text'] = '[edit_node] [delete_node]';
    $handler->display->display_options['fields']['delete_node']['element_label_colon'] = FALSE;
    /* Field: Global: Custom text */
    $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
    $handler->display->display_options['fields']['nothing']['table'] = 'views';
    $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
    $handler->display->display_options['fields']['nothing']['label'] = 'Operations';
    $handler->display->display_options['fields']['nothing']['alter']['text'] = '[edit_node] [delete_node]';
    $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
    /* Sort criterion: Content: Updated date */
    $handler->display->display_options['sorts']['changed']['id'] = 'changed';
    $handler->display->display_options['sorts']['changed']['table'] = 'node';
    $handler->display->display_options['sorts']['changed']['field'] = 'changed';
    $handler->display->display_options['sorts']['changed']['order'] = 'DESC';
    /* Filter criterion: Content access: Access */
    $handler->display->display_options['filters']['nid']['id'] = 'nid';
    $handler->display->display_options['filters']['nid']['table'] = 'node_access';
    $handler->display->display_options['filters']['nid']['field'] = 'nid';
    $handler->display->display_options['filters']['nid']['group'] = 1;
    /* Filter criterion: Content: Type */
    $handler->display->display_options['filters']['type']['id'] = 'type';
    $handler->display->display_options['filters']['type']['table'] = 'node';
    $handler->display->display_options['filters']['type']['field'] = 'type';
    $handler->display->display_options['filters']['type']['value'] = array(
        'all' => 'all',
        'miamilaw_page' => 'miamilaw_page',
        'miamilaw_right_rail' => 'miamilaw_right_rail',
        'carousel_content' => 'carousel_content',
        'admin_staff_member' => 'admin_staff_member',
        'faculty_member' => 'faculty_member',
        'miami_law_external_link' => 'miami_law_external_link',
        'miami_law_document' => 'miami_law_document',
        'miami_law_article' => 'miami_law_article',
        'miamilaw_section' => 'miamilaw_section',
    );
    $handler->display->display_options['filters']['type']['group'] = 1;
    $handler->display->display_options['filters']['type']['exposed'] = TRUE;
    $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
    $handler->display->display_options['filters']['type']['expose']['label'] = 'Show only items where type';
    $handler->display->display_options['filters']['type']['expose']['description'] = 'Select type to filter by';
    $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
    $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
    $handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
        2 => '2',
        1 => 0,
        3 => 0,
        6 => 0,
        4 => 0,
        5 => 0,
    );
    $handler->display->display_options['filters']['type']['expose']['reduce'] = TRUE;
    $handler->display->display_options['filters']['type']['group_info']['label'] = 'Type';
    $handler->display->display_options['filters']['type']['group_info']['identifier'] = 'type';
    $handler->display->display_options['filters']['type']['group_info']['remember'] = FALSE;
    $handler->display->display_options['filters']['type']['group_info']['group_items'] = array(
        1 => array(),
        2 => array(),
        3 => array(),
    );
    /* Filter criterion: Content: Title */
    $handler->display->display_options['filters']['title']['id'] = 'title';
    $handler->display->display_options['filters']['title']['table'] = 'node';
    $handler->display->display_options['filters']['title']['field'] = 'title';
    $handler->display->display_options['filters']['title']['operator'] = 'contains';
    $handler->display->display_options['filters']['title']['group'] = 1;
    $handler->display->display_options['filters']['title']['exposed'] = TRUE;
    $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
    $handler->display->display_options['filters']['title']['expose']['label'] = 'Search in Title';
    $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
    $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
    $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
        2 => '2',
        1 => 0,
        3 => 0,
        6 => 0,
        4 => 0,
        5 => 0,
    );
    $handler->display->display_options['filters']['title']['group_info']['label'] = 'Title';
    $handler->display->display_options['filters']['title']['group_info']['identifier'] = 'title';
    $handler->display->display_options['filters']['title']['group_info']['remember'] = FALSE;
    $handler->display->display_options['filters']['title']['group_info']['group_items'] = array(
        1 => array(),
        2 => array(),
        3 => array(),
    );
    /* Filter criterion: Content: Section (field_section) */
    $handler->display->display_options['filters']['field_section_tid']['id'] = 'field_section_tid';
    $handler->display->display_options['filters']['field_section_tid']['table'] = 'field_data_field_section';
    $handler->display->display_options['filters']['field_section_tid']['field'] = 'field_section_tid';
    $handler->display->display_options['filters']['field_section_tid']['exposed'] = TRUE;
    $handler->display->display_options['filters']['field_section_tid']['expose']['operator_id'] = 'field_section_tid_op';
    $handler->display->display_options['filters']['field_section_tid']['expose']['label'] = 'Section';
    $handler->display->display_options['filters']['field_section_tid']['expose']['operator'] = 'field_section_tid_op';
    $handler->display->display_options['filters']['field_section_tid']['expose']['identifier'] = 'field_section_tid';
    $handler->display->display_options['filters']['field_section_tid']['expose']['remember_roles'] = array(
        2 => '2',
        1 => 0,
        3 => 0,
        6 => 0,
        4 => 0,
        5 => 0,
    );
    $handler->display->display_options['filters']['field_section_tid']['group_info']['label'] = 'Section (field_section)';
    $handler->display->display_options['filters']['field_section_tid']['group_info']['identifier'] = 'field_section_tid';
    $handler->display->display_options['filters']['field_section_tid']['group_info']['remember'] = FALSE;
    $handler->display->display_options['filters']['field_section_tid']['group_info']['group_items'] = array(
        1 => array(),
        2 => array(),
        3 => array(),
    );
    $handler->display->display_options['filters']['field_section_tid']['type'] = 'select';
    $handler->display->display_options['filters']['field_section_tid']['vocabulary'] = 'section';

    /* Display: Page */
    $handler = $view->new_display('page', 'Page', 'page');
    $handler->display->display_options['path'] = 'admin/content/search';
    $handler->display->display_options['menu']['type'] = 'tab';
    $handler->display->display_options['menu']['title'] = 'Content (Search)';
    $handler->display->display_options['menu']['description'] = 'Search content';
    $handler->display->display_options['menu']['weight'] = '0';
    $handler->display->display_options['menu']['name'] = 'management';
    $handler->display->display_options['menu']['context'] = 0;
    $handler->display->display_options['menu']['context_only_inline'] = 0;
    $handler->display->display_options['tab_options']['weight'] = '0';

    return $view;
}

function get_status_view() {
    $view = new view();
    $view->name = 'content_by_status_';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'node';
    $view->human_name = 'Content (By Status)';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = 'Content (By Status)';
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'role';
    $handler->display->display_options['access']['role'] = array(
        3 => '3',
        5 => '5',
        4 => '4',
        6 => '6',
    );
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '25';
    $handler->display->display_options['style_plugin'] = 'table';
    /* Field: Content: Title */
    $handler->display->display_options['fields']['title']['id'] = 'title';
    $handler->display->display_options['fields']['title']['table'] = 'node';
    $handler->display->display_options['fields']['title']['field'] = 'title';
    $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
    $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
    /* Field: Content: Type */
    $handler->display->display_options['fields']['type']['id'] = 'type';
    $handler->display->display_options['fields']['type']['table'] = 'node';
    $handler->display->display_options['fields']['type']['field'] = 'type';
    /* Field: State Flow: State */
    $handler->display->display_options['fields']['state']['id'] = 'state';
    $handler->display->display_options['fields']['state']['table'] = 'node_revision_states';
    $handler->display->display_options['fields']['state']['field'] = 'state';
    /* Field: Content: Updated date */
    $handler->display->display_options['fields']['changed']['id'] = 'changed';
    $handler->display->display_options['fields']['changed']['table'] = 'node';
    $handler->display->display_options['fields']['changed']['field'] = 'changed';
    $handler->display->display_options['fields']['changed']['label'] = 'Updated';
    $handler->display->display_options['fields']['changed']['date_format'] = 'short';
    $handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
    /* Field: Content: Edit link */
    $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
    $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
    $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
    $handler->display->display_options['fields']['edit_node']['label'] = 'Operations';
    $handler->display->display_options['fields']['edit_node']['exclude'] = TRUE;
    /* Field: Content: Delete link */
    $handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
    $handler->display->display_options['fields']['delete_node']['table'] = 'views_entity_node';
    $handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
    $handler->display->display_options['fields']['delete_node']['exclude'] = TRUE;
    /* Field: Global: Custom text */
    $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
    $handler->display->display_options['fields']['nothing']['table'] = 'views';
    $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
    $handler->display->display_options['fields']['nothing']['label'] = 'Operations';
    $handler->display->display_options['fields']['nothing']['alter']['text'] = '[edit_node] [delete_node]';
    /* Sort criterion: Content: Post date */
    $handler->display->display_options['sorts']['created']['id'] = 'created';
    $handler->display->display_options['sorts']['created']['table'] = 'node';
    $handler->display->display_options['sorts']['created']['field'] = 'created';
    $handler->display->display_options['sorts']['created']['order'] = 'DESC';
    /* Filter criterion: State Flow: State */
    $handler->display->display_options['filters']['state']['id'] = 'state';
    $handler->display->display_options['filters']['state']['table'] = 'node_revision_states';
    $handler->display->display_options['filters']['state']['field'] = 'state';
    $handler->display->display_options['filters']['state']['value'] = array(
        'draft' => 'draft',
        'needs review' => 'needs review',
        'approved' => 'approved',
        'published' => 'published',
        'unpublished' => 'unpublished',
        'scheduled' => 'scheduled',
        'scheduled for unpublishing' => 'scheduled for unpublishing',
    );
    $handler->display->display_options['filters']['state']['group'] = 1;
    $handler->display->display_options['filters']['state']['exposed'] = TRUE;
    $handler->display->display_options['filters']['state']['expose']['operator_id'] = 'state_op';
    $handler->display->display_options['filters']['state']['expose']['label'] = 'State';
    $handler->display->display_options['filters']['state']['expose']['operator'] = 'state_op';
    $handler->display->display_options['filters']['state']['expose']['identifier'] = 'state';
    $handler->display->display_options['filters']['state']['expose']['remember_roles'] = array(
        2 => '2',
        1 => 0,
        3 => 0,
        6 => 0,
        4 => 0,
        5 => 0,
    );
    $handler->display->display_options['filters']['state']['group_info']['label'] = 'State';
    $handler->display->display_options['filters']['state']['group_info']['identifier'] = 'state';
    $handler->display->display_options['filters']['state']['group_info']['remember'] = FALSE;
    $handler->display->display_options['filters']['state']['group_info']['group_items'] = array(
        1 => array(),
        2 => array(),
        3 => array(),
    );
    /* Filter criterion: Content access: Access */
    $handler->display->display_options['filters']['nid']['id'] = 'nid';
    $handler->display->display_options['filters']['nid']['table'] = 'node_access';
    $handler->display->display_options['filters']['nid']['field'] = 'nid';
    /* Filter criterion: Content: Title */
    $handler->display->display_options['filters']['title']['id'] = 'title';
    $handler->display->display_options['filters']['title']['table'] = 'node';
    $handler->display->display_options['filters']['title']['field'] = 'title';
    $handler->display->display_options['filters']['title']['operator'] = 'contains';
    $handler->display->display_options['filters']['title']['exposed'] = TRUE;
    $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
    $handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
    $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
    $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
    $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
        2 => '2',
        1 => 0,
        3 => 0,
        6 => 0,
        4 => 0,
        5 => 0,
    );

    /* Display: Page */
    $handler = $view->new_display('page', 'Page', 'page');
    $handler->display->display_options['path'] = 'admin/content/status';
    $handler->display->display_options['menu']['type'] = 'tab';
    $handler->display->display_options['menu']['title'] = 'Content (By Status)';
    $handler->display->display_options['menu']['description'] = 'See all content filterable by status';
    $handler->display->display_options['menu']['weight'] = '0';
    $handler->display->display_options['menu']['name'] = 'management';
    $handler->display->display_options['menu']['context'] = 0;
    $handler->display->display_options['menu']['context_only_inline'] = 0;
    $handler->display->display_options['tab_options']['weight'] = '0';

    return $view;
}