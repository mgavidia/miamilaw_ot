<?php
/**
 * Created by PhpStorm.
 * User: mgavidia
 * Date: 2/24/15
 * Time: 2:56 PM
 */

/**
 * @file
 */

class MiamilawWorkflow extends StateFlow {
    public function init(){
        // Initialize states
        $this->create_state('draft', array(
            'label' => t('Draft'),
        ));
        $this->create_state('needs review', array(
            'label' => t('Needs Review'),
        ));
        $this->create_state('approved', array(
            'label' => t('Approved'),
        ));
        $this->create_state('unpublished', array(
            'label' => t('Unpublished'),
        ));
        $this->create_state('published', array(
            'label' => t('Published'),
            'on_enter' => array($this, 'on_enter_published'),
            'on_exit' => array($this, 'on_exit_published'),
            )
        );
        $this->create_state('scheduled', array(
            'label' => t('Scheduled'),
            'on_exit' => array($this, 'on_exit_scheduled'),
            )
        );

        $this->create_state('scheduled for unpublishing', array(
                'label' => t('Scheduled to be unpublished'),
                'on_exit' => array($this, 'on_exit_scheduled_unpublished'),
            )
        );


        // Initialize events
        $this->create_event('for review', array(
            'label' => t('For Review'),
            'origin' => 'draft',
            'target' => 'needs review',
            //'guard' => 'miamilaw_workflow_guard_contributor'
            )
        );
        $this->create_event('immediate publish', array(
            'label' => t('Immediate Publish'),
            'origin' => 'draft',
            'target' => 'published',
            'guard' => 'miamilaw_workflow_guard_publisher',
            )
        );
        $this->create_event('immediate approve', array(
            'label' => t('Immediate Approve'),
            'origin' => 'draft',
            'target' => 'approved',
            'guard' => 'miamilaw_workflow_guard_editor',
        ));
        $this->create_event('approve', array(
            'label' => t('Approve'),
            'origin' => 'needs review',
            'target' => 'approved',
            'guard' => 'miamilaw_workflow_guard_editor',
            )
        );
        $this->create_event('reject', array(
            'label' => t('Reject'),
            'origin' => array('needs review', 'approved'),
            'target' => 'draft',
            'guard' => 'miamilaw_workflow_guard_editor',
            )
        );
        $this->create_event('publish', array(
            'label' => t('Publish'),
            'origin' => array('scheduled', 'approved', 'unpublished'),
            'target' => 'published',
            'guard' => 'miamilaw_workflow_guard_publisher',
            )
        );
        $this->create_event('unpublish', array(
            'label' => t('Unpublish'),
            'origin' => array('scheduled for unpublishing', 'published'),
            'target' => 'unpublished',
            'guard' => 'miamilaw_workflow_guard_publisher',
            )
        );
        $this->create_event('to draft', array(
            'label' => t('To Draft'),
            'origin' => array('needs review', 'approved', 'unpublished', 'scheduled', 'scheduled for unpublishing'),
            'target' => 'draft',
            )
        );
        $this->create_event('schedule', array(
            'label' => t('Schedule'),
            'origin' => array('draft', 'approved', 'unpublished'),
            'target' => 'scheduled',
            'guard' => 'state_flow_guard_schedule',
            )
        );

        $this->create_event('schedule unpublish', array(
            'label' => t('Schedule to unpublish'),
            'origin' => array('published'),
            'target' => 'scheduled for unpublishing',
            'guard' => 'state_flow_guard_schedule',
            )
        );
    }

    public function on_event_fail($event){
        $key = array_search($event, $this->events);
        drupal_set_message(t('Could not transition node using %event event.'), array('%event' => $key), 'error');
    }


    public function on_exit_scheduled_unpublished() {
        db_query('
          DELETE FROM miamilaw_states_schedule_unpublished
          WHERE nid = :nid
          AND vid = :vid',
                array(
                    ':nid' => $this->object->nid,
                    ':vid' => $this->object->vid,
                )
            );
        $this->set_unpublished();
    }

    public function on_exit_published() {
        // Don't want to unpublish if we are scheduling
        $exists = db_query('
          SELECT 1 FROM miamilaw_states_schedule_unpublished
          WHERE nid = :nid
          AND vid = :vid',
            array(
                ':nid' => $this->object->nid,
                ':vid' => $this->object->vid,
            )
        );
        if (!$exists) {
            $this->set_unpublished();
        }
    }
}

/*function miamilaw_workflow_guard_contributor($event){
    return miamilaw_workflow_guard_permission($event, 'draft to review');
}*/

/**
 * Guard callback for the EnergyWorkflow approve and reject events.
 */
function miamilaw_workflow_guard_editor($event) {
    return miamilaw_workflow_guard_permission($event, 'approve and reject content');
}
/**
 * Guard callback for the EnergyWorkflow publish and unpublish events.
 */
function miamilaw_workflow_guard_publisher($event) {
    return miamilaw_workflow_guard_permission($event, 'publish and unpublish content');
}

/**
 * Helper function for evaluating an access decision with either the global or
 * group-specific permission.
 */
function miamilaw_workflow_guard_permission($event, $permission) {
    // If the user has the global permission, then return TRUE
    if (user_access($permission)) {
        return TRUE;
    }

    // Otherwise, check for the permission in the node's group
    $machine = $event->get_machine();
    $node = $machine->get_object();
    $groups = field_get_items('node', $node, 'group_audience');
    $gid = !empty($groups[0]['gid']) ? $groups[0]['gid'] : FALSE;
    if ($gid && og_user_access($gid, $permission)) {
        return TRUE;
    }

    // Otherwise, return FALSE
    return FALSE;
}


/*function state_flow_admin_nodes() {
    return array();
}*/