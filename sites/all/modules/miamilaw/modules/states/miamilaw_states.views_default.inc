<?php
function miamilaw_states_views_default_views(){
    $views = array();

    include(dirname(__FILE__) . '/views/view.miamilaw_states.inc');
    $status = get_status_view();
    $views[$status->name] = $status;

    $search = get_search_view();
    $views[$search->name] = $search;

    return $views;
}