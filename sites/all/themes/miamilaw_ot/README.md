Miami Law Omega Template Theme
==============================

Requirements
============
    * Install modules
        - views
        - views_ui
        - responsive_menus
        - libraries
        - jquery_update
    * You can install them easily by executing bootstrap.sh

Usage
=====

    * run the following
        * bundle install
    * to run the sass watch tasks
        * drush omega-guard

Additional
==========


