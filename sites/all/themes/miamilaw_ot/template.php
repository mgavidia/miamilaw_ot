<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * Law School Omega Template theme.
 */
function miamilaw_ot_preprocess_html(&$variables) {

    $libraries = drupal_get_path('theme', 'miamilaw_ot') . '/libraries/';

    // Semantic UI
    //drupal_add_css('//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.0.8/semantic.min.css', array('type' => 'external'));
    drupal_add_js($libraries . 'semantic/dist/semantic.min.js');
    drupal_add_css($libraries . 'semantic/dist/components/button.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/card.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/feed.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/header.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/icon.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/image.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/input.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/item.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/label.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/list.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/menu.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/message.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/segment.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/sidebar.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/table.min.css');
    drupal_add_css($libraries . 'semantic/dist/components/transition.min.css');
    
    //drupal_add_js('//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js', array('type' => 'external'));
    
    // Font awesome
    drupal_add_css($libraries . 'font-awesome/css/font-awesome.min.css');
    
    // Bootstrap
    //drupal_add_css('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css', array('type' => 'external'));
    //drupal_add_js('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js', array('type' => 'external'));

    // Cycle 2
    drupal_add_js($libraries . 'jquery-cycle2/build/jquery.cycle2.min.js');
    drupal_add_js($libraries . 'jquery-cycle2/build/plugin/jquery.cycle2.caption2.min.js');
    drupal_add_js($libraries . 'jquery-cycle2/build/plugin/jquery.cycle2.center.min.js');
    drupal_add_js($libraries . 'jquery-cycle2/build/plugin/jquery.cycle2.swipe.min.js');
    
    // Fit Vids
    drupal_add_js($libraries . 'fitvids/jquery.fitvids.js');
    
    // Slicknav
    drupal_add_css($libraries . 'slicknav/dist/slicknav.min.css');
    drupal_add_js($libraries . 'slicknav/dist/jquery.slicknav.min.js');
   
    // Chosen
    drupal_add_css($libraries . 'chosen/chosen.css');
    drupal_add_js($libraries . 'chosen/chosen.jquery.js');
  
    // Jquery UI
    /*drupal_add_css('//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/css/jquery-ui.min.css', array('type' => 'external'));
    drupal_add_js('//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js', array('type' => 'external'));*/

    // jw-player
    //drupal_add_js('http://jwpsrv.com/library/RcPNsMgfEeSJCAp+lcGdIw.js', array('type' => 'external'));

    // Enquire.js
    drupal_add_js($libraries . 'enquire/dist/enquire.min.js');

    // Meta
    /*$viewport = array(
        '#tag' => 'meta',
        '#attributes' => array(
            'name' => 'viewport',
            'content' => 'width=device-width, initial-scale=1',
        ),
    );
    drupal_add_html_head($viewport, 'viewport');*/
}

/**
 * Implements theme_menu_link().
 *
 * This code adds an icon <I> tag for use with icon fonts when a menu item
 * contains a CSS class that starts with "icon-". You may add CSS classes to
 * your menu items through the Drupal admin UI with the menu_attributes contrib
 * module.
 *
 * Originally written by lacliniquemtl.
 * Refactored by jwilson3.
 * @see http://drupal.org/node/1689728
 */
function miamilaw_ot_menu_link(array $variables) {
    /**
     * Implements theme_menu_link().
     *
     * This code adds an icon <I> tag for use with icon fonts when a menu item
     * contains a CSS class that starts with "icon-". You may add CSS classes to
     * your menu items through the Drupal admin UI with the menu_attributes contrib
     * module.
     *
     * Originally written by lacliniquemtl.
     * Refactored by jwilson3 > mroji28 > driesdelaey.
     * @see http://drupal.org/node/1689728
     */
    $element = $variables['element'];
    $sub_menu = '';
    // If there is a CSS class on the link that starts with "icon-", create
    // additional HTML markup for the icon, and move that specific classname there.
    // Exclusion List for settings eg http://fontawesome.io/examples/
    $exclusion = array(
        'fa-lg','fa-2x','fa-3x','fa-4x','fa-5x',
        'fa-fw',
        'fa-ul', 'fa-li',
        'fa-border',
        'fa-spin',
        'fa-rotate-90', 'fa-rotate-180','fa-rotate-270','fa-flip-horizontal','fa-flip-vertical',
        'fa-stack', 'fa-stack-1x', 'fa-stack-2x',
        'fa-inverse'
    );
    if (isset($element['#original_link']['options']['attributes']['class'])) {
        foreach ($element['#original_link']['options']['attributes']['class'] as $key => $class) {
            /*if (substr($class, 0, 2) == 'fa' && substr($class, 0, 3) != 'fa-'){
                unset($element['#localized_options']['attributes']['class'][$key]);
            } else */
            if (substr($class, 0, 3) == 'fa-' && !in_array($class,$exclusion)) {
                // We're injecting custom HTML into the link text, so if the original
                // link text was not set to allow HTML (the usual case for menu items),
                // we MUST do our own filtering of the original text with check_plain(),
                // then specify that the link text has HTML content.
                if (!isset($element['#original_link']['options']['html']) || empty($element['#original_link']['options']['html'])) {
                    $element['#title'] = check_plain($element['#title']);
                    $element['#localized_options']['html'] = TRUE;
                }
                // Add the default-FontAwesome-prefix so we don't need to add it manually in the menu attributes
                $class = 'fa ' . $class;
                // Create additional HTML markup for the link's icon element and wrap
                // the link text in a SPAN element, to easily turn it on or off via CSS.
                //$element['#title'] = '<i class="' . $class . '"></i> <span>' . $element['#title'] . '</span>';
                $element['#title'] = '';
                //unset($element['#title']);
                // Finally, remove the icon class from link options, so it is not printed twice.
                //unset($element['#localized_options']['attributes']['class'][$key]);
                // kpr($element); // For debugging.
            }
        }
    }
    // Save our modifications, and call core theme_menu_link().
    $variables['element'] = $element;
    return theme_menu_link($variables);
}

function miamilaw_ot_preprocess_search_result(&$variables) {
    $result = $variables['result'];
    if (!empty($result['date'])){
        $variables['info'] = 'Created: ' . format_date($result['date'], 'custom', 'D, F jS, Y');
    } else {
        $variables['info'] = 'Created: unknown';
    }

    // Fix links
    if ($result['node']->type == 'miami_law_external_link'){
        $variables['url'] = $result['node']->field_external_link_url[$result['node']->language][0]['url'];
    } else if ($result['node']->type == 'miami_law_document'){
        $file = file_load($result['node']->field_document_file[$result['node']->language][0]['fid']);
        $variables['url'] = file_create_url($file->uri);
    }
}
