(function ($) {

    /* Responsive values */
    var mobile = 320;
    var tablet = 768;
    var desktop = 1008;
    var wide = 1200;

    /** No foreach */
    if (!Array.prototype.forEach) {
        Array.prototype.forEach = function(fn, scope) {
	    for(var i = 0, len = this.length; i < len; ++i) {
	        fn.call(scope, this[i], i, this);
	    }
        }
    }


  /**
   * The recommended way for producing HTML markup through JavaScript is to write
   * theming functions. These are similiar to the theming functions that you might
   * know from 'phptemplate' (the default PHP templating engine used by most
   * Drupal themes including Omega). JavaScript theme functions accept arguments
   * and can be overriden by sub-themes.
   *
   * In most cases, there is no good reason to NOT wrap your markup producing
   * JavaScript in a theme function.
   */
  Drupal.theme.prototype.miamilawOtExampleButton = function (path, title) {
    // Create an anchor element with jQuery.
    return $('<a href="' + path + '" title="' + title + '">' + title + '</a>');
  };

  /**
   * Behaviors are Drupal's way of applying JavaScript to a page. In short, the
   * advantage of Behaviors over a simple 'document.ready()' lies in how it
   * interacts with content loaded through Ajax. Opposed to the
   * 'document.ready()' event which is only fired once when the page is
   * initially loaded, behaviors get re-executed whenever something is added to
   * the page through Ajax.
   *
   * You can attach as many behaviors as you wish. In fact, instead of overloading
   * a single behavior with multiple, completely unrelated tasks you should create
   * a separate behavior for every separate task.
   *
   * In most cases, there is no good reason to NOT wrap your JavaScript code in a
   * behavior.
   *
   * @param context
   *   The context for which the behavior is being executed. This is either the
   *   full page or a piece of HTML that was just added through Ajax.
   * @param settings
   *   An array of settings (added through drupal_add_js()). Instead of accessing
   *   Drupal.settings directly you should use this because of potential
   *   modifications made by the Ajax callback that also produced 'context'.
   */
  Drupal.behaviors.miamilawOtExampleBehavior = {
    attach: function (context, settings) {
      // By using the 'context' variable we make sure that our code only runs on
      // the relevant HTML. Furthermore, by using jQuery.once() we make sure that
      // we don't run the same piece of code for an HTML snippet that we already
      // processed previously. By using .once('foo') all processed elements will
      // get tagged with a 'foo-processed' class, causing all future invocations
      // of this behavior to ignore them.
      $('.some-selector', context).once('foo', function () {
        // Now, we are invoking the previously declared theme function using two
        // settings as arguments.
        var $anchor = Drupal.theme('miamilawOtExampleButton', settings.myExampleLinkPath, settings.myExampleLinkTitle);

        // The anchor is then appended to the current element.
        $anchor.appendTo(this);
      });
    }
  };

    /*

    Modify the search bar to make it look nice
     */

  Drupal.behaviors.miamilawOtSearchBehavior = {
      attach: function (context, settings) {
          var header = $('.main-navigation');
          header.once('header-search-changes', function(){
              var searchBlock = $('.l-search');
              var searchBlockMargin = searchBlock.css('margin-top');
              var form = $("[id^=edit-search-block-form--]");
              form.attr('placeholder', 'SEARCH');
              form.before('<span class="search-icon fa fa-search"></span>');
              form.after('<span title="Click to close." class="close-button fa fa-times"></span>');

              var searchIcon = $('.search-icon');
              var closeButton = $('.close-button');
              var links = $('.l-links');

              //form.css('top', 190);//(header.offset().top - header.height() ));// - form.height());
              searchIcon.css({
                  'position': 'absolute',
                  'top': '.4em',
                  'left': '2.2em'
              });
              closeButton.css({
                  'position': 'absolute',
                  'top': '.4em',
                  'right': '2em'
              });

              searchIcon.hide();
              closeButton.hide();

              form.hide();

              var block = $('.search-block-form');
              block.append('<label class="search-label"><span class="fa fa-search"></span>&nbsp;&nbsp;Search</label>');
              var label = $('.search-label');

              var showing = false;
              var fields = function(force){
                  var override = typeof(force)!=='undefined';
                  if (override || showing){
                      showing = false;
                      form.css('position', 'absolute');
                      searchBlock.css('margin-top', searchBlockMargin);
                      form.fadeOut();
                      closeButton.fadeOut();
                      searchIcon.fadeOut();
                      label.fadeIn();
                      links.show();
                  } else {
                      showing = true;
                      closeButton.fadeIn();
                      searchIcon.fadeIn();
                      label.fadeOut();
                      links.hide();
                      form.fadeIn(function(){
                          //searchBlock.css('margin-top', 0);
                          form.css({
                              'position': 'relative'
                          });
                          closeButton.css('position', 'absolute');
                      });
                      form.focus();
                  }
              };

              closeButton.click(function(){
                 fields(true);
              });

              label.click(function(){
                  fields();
              });

              form.focusout(function(){
                  fields();
              });
              /*
              form.keyup(function(e){
                  if (e.keyCode == 27){
                    fields(true);
                  }
              });*/

              /*
              var changeWidth = function(){
                  var menu = $(".l-header .menu");
                  var width = 0;
                  menu.children().each(function(){
                     width = width + $(this)[0].scrollWidth;//.width();
                  });

                  form.css('width', width);
              }

              changeWidth();

              $(window).resize(function(){
                  changeWidth();
              });*/
          });
      }
  };

   /*
    Handle the quad section of pages to lengthen them appropriately
     */
  Drupal.behaviors.miamilawOtQuadBehavior = {
      attach: function (context, settings) {
	if (typeof enquire != 'undefined'){
          enquire.register('screen', {
              match: function () {
                  if (!$("[class^=l-quad]").length) {
                      return;
                  }

                  var content = $("[class^=l-quad]");

                  var fields = $('div.column-alt');

                  var heightCheck = function () {
                      var tallest = 0;
                      var heightClass = '';
                      fields.each(function (index) {
                          //$(this).addClass('column-' + index);
                          var height = 0;
                          $(this).children().each(function () {
                              height = height + $(this).outerHeight(true);
                          });
                          if (height > tallest){
                              tallest = height;
                          }
                          //console.log('index: ' + index + ' height: ' + height);
                      });
                      //console.log('Tallest: ' + tallest + ' Class: ' + heightClass);
                      //fields.addClass(heightClass);
                      if ($(window).width() < tablet) {
                          fields.css('height', 'auto');
                      } else {
                          fields.css('height', tallest + 32);
                      }
                  };

                  heightCheck();

                  content.masonry({
                      itemSelector: '.column, .column-alt'
                  });

                  content.masonry();

                  $(window).resize(function () {
                      heightCheck();
                  });
              },
              unmatch: function(){
                  var content = $("[class^=l-quad]");
                  content.masonry('destroy');
              }
          });
	}
      }
  };

    /*
    Make page menu sticky
     */
    Drupal.behaviors.miamilawOtStickyMenuBehavior = {
        attach: function (context, settings) {
            var default_temp = $("[id^=default-template]");
            if (!default_temp.length){
                return;
            }
            $(window).bind('load', function() {
                default_temp.once('default-template', function () {
                    var content = $("[class^=l-content]");
                    var left = content.find('.left').first();
                    var l_menu = left.find('ul').first();
                    l_menu.slicknav({
                        prependTo: content,
                        label: 'MORE NAVIGATION'
                    });
                    var nav = $('.slicknav_menu .slicknav_btn');
                    $('.slicknav_menutxt', nav).appendTo(nav);

                    var setup = function () {
                        left.hcSticky({
                            //offResolutions: [-desktop]
                        });
                        var right = content.find('.right').first();
                        if (right.length) {
                            right.hcSticky({
                                //offResolutions: [-desktop]
                            });
                        }
                    };
                    var destroy = function () {
                        left.hcSticky('destroy');
                        var right = content.find('.right').first();
                        if (right.length) {
                            right.hcSticky('destroy');
                        }
                    };
                    var restart = function() {
                        left.hcSticky('on');
                        var right = content.find('.right').first();
                        if (right.length) {
                            right.hcSticky('on');
                        }
                    };
                    var stop = function () {
                        left.hcSticky('off');
                        var right = content.find('.right').first();
                        if (right.length) {
                            right.hcSticky('off');
                        }
                    };
                    setup();
                    var wrapper = $('.wrapper-sticky');
                    $(window).resize(function () {
                        if ($(window).width() < desktop) {
                            wrapper.first().css('display', 'none');
                            stop();
                            wrapper.last().css({
                                width: '',
                                float: 'left'
                            });
                        } else {
                            wrapper.first().css({
                                display: 'block',
                                float: 'left'
                            });
                            restart();
                        }
                    });

                });
            });
        }
    };

    /*
     Combine Menus
     */
    Drupal.behaviors.miamilawOtMobileMenu = {
        attach: function (context, settings) {
            var semanticMenu = $('.mobile-menu.ui.sidebar');
            var page = $('.l-page.pusher.dimmed');
            Drupal.settings.miamilaw_mobile_menu.forEach(function(item){
               semanticMenu.append('<a href="' + item.url + '" class="white item">' + item.name + '</a>');
            });

            $('[class^=mobile-menu-button]').click(function(){
                semanticMenu
                    .sidebar('toggle')
                ;
            });

            $(window).resize(function () {
               semanticMenu.sidebar('hide');
            });
        }
    };

    Drupal.behaviors.miamilawOtUpdateCarousel = {
        attach: function (context, settings){
            var cycleSlideshow = $("[class^=cycle-slideshow]");
            if (!cycleSlideshow.length){
                return;
            }

            $(window).bind('load', function() {
                cycleSlideshow.once('carousel-modifiers', function () {

                    var slides = $('img', cycleSlideshow).length;

                    // Carousel
                    var carousel = $('.l-carousel');

                    // Pagers
                    var prev = $('.control-prev');
                    var next = $('.control-next');
                    var pager = $('.cycle-pager');

                    var hasControls = true;

                    if (slides <= 1) {
                        prev.css('display', 'none');
                        next.css('display', 'none');
                        pager.css('display', 'none');
                        hasControls = false;
                    } else {
                        prev.css('display', 'block');
                        next.css('display', 'block');
                        pager.css('display', 'block');
                    }

                    $('.cycle-slideshow').on('cycle-initialized', function (e, opts, API) {
                    });

                    var updateCarousel = function () {
                        var currentWidth = $(window).width();
                        var position = (currentWidth - pager.width()) / 2;

                        // Center the pager on the bottom
                        pager.css({
                            'margin-left': position
                        });

                        // Overlay
                        var overlay = $('.cycle-overlay');

                        if (currentWidth < tablet && hasControls) {
                            pager.css('display', 'none');
                        } else if (currentWidth >= tablet && hasControls) {
                            pager.css('display', 'block');
                        }
                    };

                    updateCarousel();
                    $(window).resize(function () {
                        updateCarousel();
                    });
                });
            });
        }
    };

    Drupal.behaviors.miamilawOtAlignSocialMenu = {
        attach: function (context, settings){
            /*if ($(window).width() < tablet){
                return;
            }*/

            var menu = $('.l-footer').find('.menu');
            menu.css('padding-left', '1em');
            /*
            menu.css({
                'position': 'absolute',
                'margin-left': ($(window).width()-menu.width())/2
            });*/
        }
    };

    /** Fitvids */
    Drupal.behaviors.miamilawOtFitVids = {
        attach: function(context, settings){
            var files = [$('.media-youtube-video'), $('.media-vimeo-video')];
            files.forEach(function(item){
               item.once(item.attr('class') + '-fitvid', function(){
                   item.fitVids();
               });
            });
            var areas = [$('.content'), $('.right')];
            areas.forEach(function(item) {
                item.fitVids();
            });
        }
    };

    /** Filtered guidelines */
    Drupal.behaviors.miamilawOtFilterGuidelines = {
        attach: function(context, settings){
            /*$('.filter-guidelines').each(function(){
                alert('Found it');
                var item = $(this);
                item.attr('data-role', 'collapsible');

            });*/
        }
    };

    /** Add chosen to all select lists  */
    Drupal.behaviors.miamilawOtChosenLibrary = {
        attach: function(context, settings){
            $(window).bind('load', function() {
                $('.form-select').once('chosen', function () {
                    $('.form-select').chosen({
                        no_results_text: 'Item does not exist.',
                        width: 'auto',
                        search_contains: true
                    });
                });
                $('.form-select-no-search').once('chosen-no-search', function () {
                    $('.form-select-no-search').chosen({
                        disable_search: true,
                        width: 'auto'
                    });
                });
            });
        }
    };


    /** Autoheight of l-header */
    /*Drupal.behaviors.miamilawOtHeaderAutoHeight = {
        attach: function(context, settings){
            var header = $('.l-header.auto-height');
            if (!header.length){
                //console.log('not found.');
                return;
            }
            $(window).bind('load', function() {
                header.once('Header Auto Height', function () {
                    var navigation = $('.main-navigation');
                    var update = function () {
                        var height = navigation.height();
                        header.css('height', height + 10);
                        //console.log('Current height: ' + height + 10);
                    };

                    update();
                    $(window).resize(function () {
                        update();
                    });
                });
            });
        }
    };*/

    /** Map resizer */
    Drupal.behaviors.resizeMaps = {
        attach: function(context, settings){
            var maps = $('map');
            if (maps.length) {
                maps.imageMapResize();
            }
        }
    };

    Drupal.behaviors.alertMessage = {
        attach: function(context, settings){
            if (Drupal.settings.miamilaw_messages.enabled){
                var url = window.location.href;
                var host = window.location.host;
                if (url.indexOf(host + Drupal.settings.miamilaw_messages.location) != -1){
                    var title = Drupal.settings.miamilaw_messages.title;
                    var content = Drupal.settings.miamilaw_messages.message;
                    var notification = $('<div class="ui large warning message transition hide" style="margin: 0;"><i class="close icon"></i><div class="header">' + title + '</div>' + content + '</div>');
                    var main = $('.l-header');
                    if (main.length == 0) {
                        main = $('.l-main');
                    }
                    main.after(notification);
                    $('.message .close').on('click', function(){
                        $(this).closest('.message').fadeOut(500);
                    });
                }
            }
            if ('miamilaw_emergency_broadcast' in Drupal.settings && Drupal.settings.miamilaw_emergency_broadcast.enabled){
                var messageTitle = Drupal.settings.miamilaw_emergency_broadcast.title;
                var message = Drupal.settings.miamilaw_emergency_broadcast.message;
                var broadcast = $('<div class="ui red large negative message transition hide" style="margin: 0;"><i class="close icon"></i><div class="header">' + messageTitle + '</div>' + message + '</div>');
                var page = $('.l-page');
                page.before(broadcast);
                $('.message .close').on('click', function(){
                    $(this).closest('.message').transition('fade');
                });
            }
        }
    };

    /** jwplayer test
    Drupal.behaviors.jwplayer = {
        attach: function(context, settings){
            var player = $('#jwplayer');
            if (!player.length){
                //console.log('not found.');
                return;
            }
            var height = player.attr('data-height') ? player.attr('data-height') : 360;
            var width = player.attr('data-width') ? player.attr('data-width') : 640;
            jwplayer('jwplayer').setup({
                file: player.attr('data-file'),
                height: height,
                width: width,
                playlist: [{

                    sources: [{
                        file: "rtmp://video.law.miami.edu/vod/" + player.attr('data-file')
                    },{
                        file: "http://video.law.miami.edu:1935/vod/" + player.attr('data-file') + "/playlist.m3u8"
                    }]
                }],

                title: player.attr('data-title'),
                image: "http://www.law.miami.edu/video/img/img-miamilaw-logo-720p.png",
                primary: "html5",
                autostart: false

            });

        }
    };*/

})(jQuery);
