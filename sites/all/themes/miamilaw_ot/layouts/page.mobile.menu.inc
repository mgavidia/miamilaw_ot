<?php
/**
 * Created by PhpStorm.
 * User: mgavidia
 * Date: 10/21/15
 * Time: 4:04 PM
 */
?>

<div class="mobile-menu ui grey sidebar inverted left vertical menu">
    <!--
    <div class="white item" style="padding: 0;">
        <form class="ui form" action="search/node" method="post">
            <div class="ui labeled inverted fluid input">
                <div class="ui grey label" style="border-radius: 0; border-right: 2px solid #7B7B7B; background-color: #666 !important;">
                    <i class="white search icon"></i>
                </div>
                <input id="mobile-search" type="text" placeholder="SEARCH" name="keys" style="border-radius: 0; background-color: #666 !important; color: white !important;">
            </div>
        </form>
    </div>
    -->
    <!--<a href="/search/node" class="white item"><i class="white search icon" style="float: left;"></i> SEARCH</a>-->
    <div class="white item" style="padding: 0;">
        <form class="ui form" action="/search/node" method="post">
            <div class="ui labeled inverted fluid input">
                <button class="ui grey label" style="border-radius: 0; border-right: 2px solid #7B7B7B; background-color: #666 !important;">
                    <i class="white search icon"></i>
                </button>
                <button id="mobile-search" type="submit" name="keys" style="width: 100%; border-radius: 0; background-color: #666 !important; color: white !important;">SEARCH</button>
            </div>
        </form>
    </div>
</div>