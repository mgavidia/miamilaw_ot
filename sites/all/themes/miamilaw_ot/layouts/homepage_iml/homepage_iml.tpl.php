<?php include dirname(__FILE__) . '/../page.mobile.menu.inc'; ?>
<div class="l-page pusher">

    <?php include dirname(__FILE__) . '/../page.nav.inc'; ?>

    <div class="l-main">
        <div class="l-iml" role="iml-body">
            <?php print render($page['iml']); ?>
        </div>
    </div>

    <?php include dirname(__FILE__) . '/../page.footer.inc'; ?>
</div>
