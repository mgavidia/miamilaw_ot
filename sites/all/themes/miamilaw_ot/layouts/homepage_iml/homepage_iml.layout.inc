name = Homepage IML
description = Main layout for the homepage (In Miami Law)
template = homepage_iml
preview = preview.png

; Regions
regions[header] = Header
regions[navigation-left] = Navigation Left
regions[navigation-right] = Navigation Right
regions[help] = Help
regions[content] = Content
regions[footer] = Footer
regions[footer-body] = Footer Body

; Additional Regions
regions[iml] = IML Body
regions[search] = Search
regions[social] = Social Links
regions[utility-links] = Utility Links

; Styles
stylesheets[all][] = css/layouts/homepage_iml/homepage_iml.layout.css
stylesheets[all][] = css/layouts/homepage_iml/homepage_iml.layout.no-query.css

; Themes
; stylesheets[all][] = css/layouts/homepage_iml/homepage_iml.layout.css
