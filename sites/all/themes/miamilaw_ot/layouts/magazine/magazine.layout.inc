name = Magazine Page(s)
description = Layout for magazine pages
template = magazine
preview = preview.png

; Regions
regions[header] = Header
regions[navigation-left] = Navigation Left
regions[navigation-right] = Navigation Right
regions[help] = Help
regions[content] = Content
regions[footer] = Footer
regions[footer-body] = Footer Body

; Additional Regions
regions[search] = Search
regions[social] = Social Links
regions[utility-links] = Utility Links
regions[content-breadcrumb] = Content Breadcrumb
regions[content-left] = Content Left
regions[content-title] = Content Title
regions[content-feature-story] = Content Feature Story
regions[content-links-social] = Content Links and Social
regions[content-stories] = Content Stories
regions[content-additional] = Content Additional

; Styles
stylesheets[all][] = css/layouts/magazine/magazine.layout.css
stylesheets[all][] = css/layouts/magazine/magazine.layout.no-query.css
