<?php include dirname(__FILE__) . '/../page.mobile.menu.inc'; ?>
<div class="l-page pusher">

    <?php include dirname(__FILE__) . '/../page.header.inc'; ?>

    <div class="l-main">
        <div class="l-content" role="main">
            <div id="default-template"><!-- Ignored --></div>
            <div id="slicknavable" class="left">
                <?php print render($page['content-left']); ?>
            </div>
            <div class="center">
                <div class="breadcrumb">
                    <?php print render($page['content-breadcrumb']); ?>
                </div>
                <div class="title">
                    <?php print render($page['content-title']); ?>
                </div>
                <div class="content">
                    <!-- Basic content -->
                    <?php print render($page['content']); ?>
                    <!-- Feature Story -->
                    <?php print render($page['content-feature-story']); ?>
                    <!-- Social Links -->
                    <?php print render($page['content-links-social']); ?>
                    <!-- Stories -->
                    <?php print render($page['content-stories']); ?>
                    <!-- Additional Stories or Content -->
                    <?php print render($page['content-additional']); ?>
                </div>
            </div>
            <div class="right">
                <?php print render($page['help']); ?>
                <?php print render($page['content-right']); ?>
            </div>
        </div>
        <div class="l-impact" role="impact">
            <?php print render($page['impact']); ?>
        </div>
    </div>

    <?php include dirname(__FILE__) . '/../page.footer.inc'; ?>
</div>
