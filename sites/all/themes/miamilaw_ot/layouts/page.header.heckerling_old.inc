<?php
/**
 * HEADER Content
 * Created by PhpStorm.
 * User: mgavidia
 * Date: 03/08/16
 * Time: 10:25 AM
 */
?>

<div class="l-heckerling-logo">
    <?php print render($page['header']); ?>
</div>

<div class="l-heckerling-navigation">
    <div class="heckerling-nav">
        <?php print render($page['navigation']); ?>
    </div>
</div>

<!--
<div class="main-navigation">
    <div class="l-branding">
        <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>

        <?php if ($site_name || $site_slogan): ?>
            <?php if ($site_name): ?>
                <h1 class="site-name">
                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                </h1>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
                <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
            <?php endif; ?>
        <?php endif; ?>

        <?php print render($page['branding']); ?>
    </div>

</div>
-->