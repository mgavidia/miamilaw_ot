<div class="l-page pusher">

    <?php include dirname(__FILE__) . '/../page.header.heckerling_old.inc'; ?>

    <div class="l-main">
        <div class="l-content" role="main">
            <div id="default-template"><!-- Ignored --></div>
            <div id="slicknavable" class="left">
                <?php print render($page['content-left']); ?>
            </div>
            <div class="center">
                <div class="title">
                    <?php print render($page['content-title']); ?>
                </div>
                <div class="breadcrumb">
                    <?php print render($page['content-breadcrumb']); ?>
                </div>
                <div class="content">
                    <a id="main-content"></a>
                    <?php print render($title_prefix); ?>
                    <?php if ($title): ?>
                        <h1><?php print $title; ?></h1>
                    <?php endif; ?>
                    <?php print render($title_suffix); ?>
                    <?php print $messages; ?>
                    <?php print render($tabs); ?>
                    <?php if ($action_links): ?>
                        <ul class="action-links"><?php print render($action_links); ?></ul>
                    <?php endif; ?>
                    <?php print render($page['content']); ?>
                    <?php print $feed_icons; ?>
                </div>
            </div>
        </div>
    </div>

    <?php include dirname(__FILE__) . '/../page.footer.inc'; ?>
</div>
