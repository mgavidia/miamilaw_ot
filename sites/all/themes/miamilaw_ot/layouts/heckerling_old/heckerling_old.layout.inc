name = Old Heckerling Theme Page
description = Old Heckerling layout for page content
template = heckerling_old
preview = preview.png

; Regions
regions[header] = Header
regions[navigation] = Navigation
regions[help] = Help
regions[content] = Content
regions[footer] = Footer
regions[footer-body] = Footer Body

; Additional Regions
regions[content-left] = Content Left
regions[content-title] = Content Title
regions[content-breadcrumb] = Content Breadcrumb
regions[utility-links] = Utility Links

; Styles
stylesheets[all][] = css/layouts/heckerling_old/heckerling_old.layout.css
stylesheets[all][] = css/layouts/heckerling_old/heckerling_old.layout.no-query.css
