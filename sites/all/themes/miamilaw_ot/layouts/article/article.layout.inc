name = News Aricle Page
description = Default layout for news and articles
template = article
preview = preview.png

; Regions
regions[header] = Header
regions[navigation-left] = Navigation Left
regions[navigation-right] = Navigation Right
regions[help] = Help
regions[content] = Content
regions[footer] = Footer
regions[footer-body] = Footer Body

; Additional Regions
regions[search] = Search
regions[impact] = Impact
regions[social] = Social Links
regions[content-left] = Content Left
regions[content-right] = Content Right
regions[content-title] = Content Title
regions[content-breadcrumb] = Content Breadcrumb
regions[content-author] = Content Author
regions[content-created] = Content Created
regions[utility-links] = Utility Links
regions[content-tags] = Content Tags

; Styles
stylesheets[all][] = css/layouts/article/article.layout.css
stylesheets[all][] = css/layouts/article/article.layout.no-query.css
