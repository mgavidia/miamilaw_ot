name = Faculty Page
description = Default layout for faculty pages
template = faculty
preview = preview.png

; Regions
regions[header] = Header
regions[navigation-left] = Navigation Left
regions[navigation-right] = Navigation Right
regions[help] = Help
regions[content] = Content
regions[footer] = Footer
regions[footer-body] = Footer Body

; Additional Regions
regions[search] = Search
regions[impact] = Impact
regions[social] = Social Links
regions[content-center-left] = Content Center Left
regions[content-center-right] = Content Center Right
regions[content-left] = Content Left
regions[content-right] = Content Right
regions[content-breadcrumb] = Content Breadcrumb
regions[utility-links] = Utility Links

; Styles
stylesheets[all][] = css/layouts/faculty/faculty.layout.css
stylesheets[all][] = css/layouts/faculty/faculty.layout.no-query.css
