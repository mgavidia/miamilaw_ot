<?php include dirname(__FILE__) . '/../page.mobile.menu.inc'; ?>
<div class="l-page pusher">

    <?php include dirname(__FILE__) . '/../page.header.inc'; ?>

    <div class="l-main">
        <div class="l-content" role="main">
            <div id="default-template"><!-- Ignored --></div>
            <div class="content">
                <a id="main-content"></a>
                <?php print $messages; ?>
                <?php print render($tabs); ?>
                <?php if ($action_links): ?>
                    <ul class="action-links"><?php print render($action_links); ?></ul>
                <?php endif; ?>
                <?php print render($page['content']); ?>
                <?php print $feed_icons; ?>
            </div>
            <div id="slicknavable" class="left">
                <?php print render($page['content-left']); ?>
            </div>
            <div class="center-left">
                <div class="breadcrumb">
                    <?php print render($page['content-breadcrumb']); ?>
                </div>
                <?php print render($page['content-center-left']); ?>
            </div>
            <div class="center-right">
                <?php print render($page['content-center-right']); ?>
            </div>
            <div class="right">
                <?php print render($page['help']); ?>
                <?php print render($page['content-right']); ?>
            </div>
        </div>
        <div class="l-impact" role="impact">
            <?php print render($page['impact']); ?>
        </div>
    </div>

    <?php include dirname(__FILE__) . '/../page.footer.inc'; ?>
</div>
