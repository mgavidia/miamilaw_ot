name = Directory Page
description = Directory layout for faculty and admin pages
template = directory
preview = preview.png

; Regions
regions[header] = Header
regions[navigation-left] = Navigation Left
regions[navigation-right] = Navigation Right
regions[help] = Help
regions[content] = Content
regions[footer] = Footer
regions[footer-body] = Footer Body

; Additional Regions
regions[search] = Search
;regions[carousel] = Carousel
;regions[impact] = Impact
regions[social] = Social Links
regions[content-left] = Content Left
regions[content-title] = Content Title
regions[content-breadcrumb] = Content Breadcrumb
regions[utility-links] = Utility Links

; Styles
stylesheets[all][] = css/layouts/directory/directory.layout.css
stylesheets[all][] = css/layouts/directory/directory.layout.no-query.css