name = Section Page
description = Main layout for the section landing homepages
template = section
preview = preview.png

; Regions
regions[header] = Header
regions[navigation-left] = Navigation Left
regions[navigation-right] = Navigation Right
regions[help] = Help
regions[content] = Content
regions[footer] = Footer
regions[footer-body] = Footer Body

; Additional Regions
regions[search] = Search
regions[carousel] = Carousel
regions[quad_content] = Quad Column Content
regions[impact] = Impact
regions[social] = Social Links
regions[utility-links] = Utility Links

; Styles
stylesheets[all][] = css/layouts/section/section.layout.css
stylesheets[all][] = css/layouts/section/section.layout.no-query.css