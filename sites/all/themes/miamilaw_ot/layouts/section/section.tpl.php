<?php include dirname(__FILE__) . '/../page.mobile.menu.inc'; ?>
<div class="l-page pusher">

    <?php include dirname(__FILE__) . '/../page.header.inc'; ?>

    <div class="l-main">
        <div class="l-carousel" role="carousel">
            <?php print render($page['carousel']); ?>
        </div>
        <div class="l-quad" role="quad">
            <?php print render($page['quad_content']); ?>
        </div>
        <div class="l-impact" role="impact">
            <?php print render($page['impact']); ?>
        </div>
    </div>

    <?php include dirname(__FILE__) . '/../page.footer.inc'; ?>
</div>
