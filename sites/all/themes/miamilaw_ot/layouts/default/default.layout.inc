name = Default Page
description = Default layout for regular page and content
template = default
preview = preview.png

; Regions
regions[header] = Header
regions[navigation-left] = Navigation Left
regions[navigation-right] = Navigation Right
regions[help] = Help
regions[content] = Content
regions[footer] = Footer
regions[footer-body] = Footer Body

; Additional Regions
regions[search] = Search
;regions[carousel] = Carousel
regions[impact] = Impact
regions[social] = Social Links
regions[content-left] = Content Left
regions[content-right] = Content Right
regions[content-title] = Content Title
regions[content-breadcrumb] = Content Breadcrumb
regions[utility-links] = Utility Links

; Styles
stylesheets[all][] = css/layouts/default/default.layout.css
stylesheets[all][] = css/layouts/default/default.layout.no-query.css
