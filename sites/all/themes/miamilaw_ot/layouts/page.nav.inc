<?php
/**
 * Navigation Content
 * Created by PhpStorm.
 * User: mgavidia
 * Date: 10/24/14
 * Time: 10:25 AM
 */
?>

<div class="mobile-header">
    <div class="left">
        <a class="mobile-menu-button"><i class="fa fa-bars fa-2x"></i>
            &nbsp;
        </a>
    </div>
    <div class="right">
        <?php print render($page['header']); ?>
    </div>
</div>

<div class="utility-bar">
    <div class="l-search">
        <?php print render($page['search']); ?>
    </div>
    <div class="l-links">
        <?php print render($page['utility-links']); ?>
    </div>
</div>

<div class="main-navigation">
    <div class="l-branding">
        <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>

        <?php if ($site_name || $site_slogan): ?>
            <?php if ($site_name): ?>
                <h1 class="site-name">
                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                </h1>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
                <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
            <?php endif; ?>
        <?php endif; ?>

        <?php print render($page['branding']); ?>
    </div>

    <div class="l-navigation-group">
        <div class="left">
            <?php print render($page['navigation-left']); ?>
        </div>
        <div class="logo">
            <?php print render($page['header']); ?>
        </div>
        <div class="right">
            <?php print render($page['navigation-right']); ?>
        </div>
    </div>
</div>