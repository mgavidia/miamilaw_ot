<?php
/**
 * FOOTER CONTENT
 * Created by PhpStorm.
 * User: mgavidia
 * Date: 8/27/14
 * Time: 10:25 AM
 */

?>

<footer class="l-footer" role="contentinfo">
    <?php print render($page['footer']); ?>
    <?php print render($page['footer-body']); ?>
    <div class="l-footer-social">
        <?php print render($page['social']); ?>
    </div>
</footer>