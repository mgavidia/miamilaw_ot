name = Homepage
description = Main layout for the homepage
template = homepage
preview = preview.png

; Regions
regions[header] = Header
regions[navigation-left] = Navigation Left
regions[navigation-right] = Navigation Right
regions[help] = Help
regions[content] = Content
regions[footer] = Footer
regions[footer-body] = Footer Body

; Additional Regions
regions[search] = Search
regions[carousel] = Carousel
regions[impact] = Impact
regions[events] = Events
regions[media-left] = Media Left
regions[media-right] = Media Right
regions[social] = Social Links
regions[utility-links] = Utility Links

; Styles
stylesheets[all][] = css/layouts/homepage/homepage.layout.css
stylesheets[all][] = css/layouts/homepage/homepage.layout.no-query.css

; Themes
; stylesheets[all][] = css/layouts/homepage/homepage.layout.css