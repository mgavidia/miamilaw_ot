<?php include dirname(__FILE__) . '/../page.mobile.menu.inc'; ?>
<div class="l-page pusher">

    <?php include dirname(__FILE__) . '/../page.nav.inc'; ?>

    <div class="l-main">
        <div class="l-carousel" role="carousel">
            <?php print render($page['carousel']); ?>
        </div>
        <div class="l-impact" role="impact">
            <?php print render($page['impact']); ?>
        </div>
        <div class="l-events" role="events">
            <?php print render($page['events']); ?>
        </div>
        <div class="l-media" role="media">
            <div class="left">
                <?php print render($page['media-left']); ?>
            </div>
            <div class="right">
                <?php print render($page['media-right']); ?>
            </div>
        </div>
    </div>

    <?php include dirname(__FILE__) . '/../page.footer.inc'; ?>
</div>
