<div class="l-page">
    <div class="l-main">
        <div class="l-login">
            <div class="l-pre-content">
                <?php print render($page['login-precont']); ?>
                <!--<div class="l-logo">
                    <?php //print render($page['login-logo']); ?>
                    <img src="sites/default/files/media/footer-logo_0.png">
                </div>
                <div class="l-info">
                    <?php //print render($page['login-info']); ?>
                    <p>
                        Stuff About how to login!
                    </p>
                </div>-->
            </div>
            <div class="l-content">
                <?php print render($title_prefix); ?>
                <?php if ($title): ?>
                    <h1><?php print $title; ?></h1>
                <?php endif; ?>
                <?php print render($title_suffix); ?>
                <?php print $messages; ?>
                <?php print render($tabs); ?>
                <?php if ($action_links): ?>
                    <ul class="action-links"><?php print render($action_links); ?></ul>
                <?php endif; ?>
                <?php print render($page['content']); ?>
                <?php print $feed_icons; ?>
            </div>
            <?php global $user; if ($user->uid): ?>
                <div class="l-account">
                    <?php print l(t('Edit Account Information'), "user/{$GLOBALS['user']->uid}/edit"); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
