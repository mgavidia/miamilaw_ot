name = Login and User Page
description = Login layout for regular page and content
template = login
preview = preview.png

; Regions
regions[help] = Help
regions[content] = Content
regions[login-precont] = Login Logo and Info (Pre-Content)

; Styles
stylesheets[all][] = css/layouts/login/login.layout.css
stylesheets[all][] = css/layouts/login/login.layout.no-query.css
