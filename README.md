# README #

This is the University of Miami Drupal 7 Omega theme and associated modules.

### Setting up for development ###

* cd into drupal7 directory
  - Ensure there is no .git related files if you setup from drupal master
* Run the following commands:
  - git init
  - git remote add origin https://username@bitbucket.org/mgavidia/miamilaw_ot.git
  - git config --add branch.master.remote origin
  - git config --add branch.master.merge refs/heads/master
  - git pull origin master

### Deploying ###

* cd into drupal7 directory
* untar the tarball
* etc

### Authored by ###

* Miguel A. Gavidia
