#!/bin/bash

drupalversion=7.28
filename=drupal-${drupalversion}.tar.gz

sitename="University of Miami School of Law"
drupaladmin=admin
drupaladminpass=admin
mysqlurl=localhost
mysqluser=root
mysqlpass=root
mysqldb=lawschool_drupal

echo "Downloading Drupal"
if [ ! -f ${filename} ]; then
	wget http://ftp.drupal.org/files/projects/${filename}
fi
tar --strip-components=1 -xvzf ${filename}
#drush dl drupal-${drupalversion} --destination=".." --drupal-project-rename="$(basename `pwd`)"
drush site-install standard --account-name=${drupaladmin} --account-pass=${drupaladminpass} --db-url=mysql://${mysqluser}:${mysqlpass}@${mysqlurl}/${mysqldb} --site-name="${sitename}" -y

echo "Restoring .gitignore"
git checkout .gitignore

echo "Disabling overlay, toolbar and other modules"
drush dis overlay toolbar -y

echo "Installing required modules..."
drush en link features panels panels_everywhere panelizer fences omega rabbit_hole rh_node taxonomy_access_fix menu_admin_per_menu -y
drush en entityreference email views views_ui views_slideshow jquery_update libraries content_access acl -y
drush en file_entity media fontyourface easy_breadcrumb token pathauto menu_attributes flickr_block flickr ckeditor ckeditor_media -y
drush en date termcase module_filter admin_menu simplified_formats remote_stream_wrapper media_internet media_vimeo media_youtube -y
drush en menu_icons special_menu_items readonlymode pathauto_persist state_machine ckeditor_link term_merge -y
# drush dl rules workflow collapse_text plupload -y
# Removed from enabled list workflow workflow_field workflow_admin_ui

echo "Installing components for omega theme."
(cd sites/all/themes/miamilaw_ot; bundle install)

echo "Grabbing jquery.cycle for views_slideshow."
mkdir -p sites/all/libraries/jquery.cycle
wget http://malsup.github.com/jquery.cycle.all.js
mv jquery.cycle.all.js sites/all/libraries/jquery.cycle

echo "Patching media browser (https://www.drupal.org/node/2089697)"
wget https://www.drupal.org/files/media.library.js_.patch
mv media.library.js_.patch sites/all/modules/media/js/plugins
(cd sites/all/modules/media/js/plugins; patch < media.library.js_.patch)

echo "Disabling base theme and enabling miamilaw_ot..."
drush pm-enable miamilaw_ot -y
drush vset theme_default miamilaw_ot -y
drush pm-disable bartik -y

echo "Disabling site wide comments for nodes."
drush dis comment -y 

echo "Clearing caches..."
drush cc all -y
